package ownerActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;
import utility.PasswordEncryption;
import utility.SendMail;
import validationActions.AddPassangerValidation;

public class CreateAccountPassanger {
	
	String name;
	String mail;
	String phone_no;
	
	public void createAccountPassanger(){
		
		
		try {
			
			String status;
			boolean userNameAvailable = false;
			HashMap<String, String> validation = new HashMap<String, String>();
			CreateAccountPassanger createAccountPassanger = new CreateAccountPassanger();
			AddPassangerValidation addPassangerValidation = new AddPassangerValidation();
			JSONObject json = new JSONObject();
			
			
			createAccountPassanger.setName(name);
			createAccountPassanger.setMail(mail);
			createAccountPassanger.setPhone_no(phone_no);
			
			validation = addPassangerValidation.passangerValditaion(createAccountPassanger);
			status = validation.get(ConstantValues.STATUS);
			
			if (status.equals(ConstantValues.SUCCESS)) {
				
				userNameAvailable = UserNameAvailable(createAccountPassanger.getMail());
				if (userNameAvailable) {
					String passangerAccount = createpassangerAccount(createAccountPassanger);
					System.out.println(passangerAccount);
				 	if (passangerAccount.equals(ConstantValues.SUCCESS)) {
				 		json.put(ConstantValues.STATUS, "SUCCESS");
					}else{
						json.put(ConstantValues.STATUS, passangerAccount);
					}
				}else{
					json.put(ConstantValues.STATUS, "MAIL_NOT_AVAILABLE");
				}
			 }else{
				 json.put(ConstantValues.STATUS, status);
			 }	
			
			 HttpServletResponse response = ServletActionContext.getResponse();
		     response.setContentType("application/json");
		     response.setCharacterEncoding("UTF-8");
		     PrintWriter writer = response.getWriter();
		     writer.write(json.toString());
		     writer.flush();
		     writer.close();
					
				
		} catch (Exception e) {
			System.out.println("Error");
		}
		
		
	}
	
	public String createpassangerAccount(CreateAccountPassanger createAccountPassanger) throws SQLException{
		
		String generation_Password = null;
		String account_password    = null;
		int passanger_id           = 0;
		boolean sendmail           = false;
		
		
		Connection conn = DatabaseConnection.getConnection();
		
		generation_Password = PasswordEncryption.generateAccountPassword();
		account_password =PasswordEncryption.encryption(generation_Password);
			
		sendmail = SendMail.sendingmail(account_password, createAccountPassanger.getMail(), createAccountPassanger.getName());
		
		if (sendmail) {
			
			PreparedStatement FetchMaxAccountIdUserRole = conn.prepareStatement(ConstantValues.FETCH_MAX_USERROLE_ID );
			ResultSet FetchUserRoleResult = FetchMaxAccountIdUserRole.executeQuery();
			
			while (FetchUserRoleResult.next()) {
				
				passanger_id = FetchUserRoleResult.getInt("maxId");
				
			}
			
			if (passanger_id == 0) {
				passanger_id = passanger_id+1010;
			}else{
				passanger_id = passanger_id+1;
			}
			
			System.out.println(passanger_id);
			
			PreparedStatement InsertIntoUserRole = conn.prepareStatement(ConstantValues.INSERT_INTO_USERROLE);
			InsertIntoUserRole.setString(1,createAccountPassanger.getName());
		    InsertIntoUserRole.setString(2,account_password);
		    InsertIntoUserRole.setString(3,ConstantValues.ROLE_PASSANGER);
	        InsertIntoUserRole.executeUpdate();
				
	        PreparedStatement InsertIntoPassangerPersonalDetails = conn.prepareStatement(ConstantValues.INSERT_INTO_PASSANGER_DETAILS);
		    InsertIntoPassangerPersonalDetails.setInt(1,passanger_id);
		    InsertIntoPassangerPersonalDetails.setString(2,account_password);
	        InsertIntoPassangerPersonalDetails.setString(3,createAccountPassanger.getName());
	        InsertIntoPassangerPersonalDetails.setString(4,createAccountPassanger.getMail());
	        InsertIntoPassangerPersonalDetails.setString(5,createAccountPassanger.getPhone_no());
		    InsertIntoPassangerPersonalDetails.executeUpdate();
		       
		    
		    return ConstantValues.SUCCESS;
		    
		}else{
			return "WRONG_MAIL";
		}
	        
		
	}
	
	
	public boolean UserNameAvailable(String username) throws SQLException{
		
		
		boolean returnValue;
		Connection conn = DatabaseConnection.getConnection();
		PreparedStatement UserNameAvailableCheck =conn.prepareStatement(ConstantValues.USERNAME_AVAILABLE_CHECK);
		UserNameAvailableCheck.setString(1, username);
		
		ResultSet UserNameAvailableResult = UserNameAvailableCheck.executeQuery();
		returnValue = UserNameAvailableResult.next();
		
		if (returnValue) {
			return false;
		}else{
			return true;
		}
		
		
	}
	
	
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	
	

}
