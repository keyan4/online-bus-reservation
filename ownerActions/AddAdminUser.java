package ownerActions;


import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;
import utility.PasswordEncryption;
import utility.SendMail;
import validationActions.AddAdminUserValidation;

public class AddAdminUser{
	
	String name;
	String mail;
	String travels_name;
	String phone_no;
	String bankName;
	String accountNumber;
	String cardNumber;
	String expiryDate;
	
	
	public void addAdminUser(){
		
		try {
			
			String status;
			boolean userNameAvailable = false;
			AddAdminUserValidation addAdminUserValidation = new AddAdminUserValidation();
			HashMap<String, String> validation = new HashMap<String, String>();
			JSONObject json = new JSONObject();
			AddAdminUser addAdminUser = new AddAdminUser();
			addAdminUser.setName(name);
			addAdminUser.setMail(mail);
			addAdminUser.setPhone_no(phone_no);
			addAdminUser.setTravels_name(travels_name);
			addAdminUser.setBankName(bankName);
			addAdminUser.setAccountNumber(accountNumber);
			addAdminUser.setCardNumber(cardNumber);
			addAdminUser.setExpiryDate(expiryDate);
			
			validation  = addAdminUserValidation.addminuserValditaion(addAdminUser);
			
			status = validation.get("status");
			
			if (status.equals("SUCCESS")) {
				userNameAvailable = UserNameAvailable(addAdminUser.getMail());
				if (userNameAvailable) {
					String addAdminuser = addadminuser(addAdminUser);
				 	if (addAdminuser.equals(ConstantValues.SUCCESS)) {
				 		json.put("status", "SUCCESS");
					}else{
						json.put("status", addAdminuser);
					}
				}else{
					json.put("status", "userNameAvailable");
				}
			 }else{
				 json.put("status", status);
			 }	
			
			HttpServletResponse response = ServletActionContext.getResponse();
		    response.setContentType("application/json");
		    response.setCharacterEncoding("UTF-8");
		    PrintWriter writer = response.getWriter();
		    writer.write(json.toString()); 
		    writer.flush();
		    writer.close();
			
		} catch (Exception e) {
			
		}
		
		
		}
		
		
		public String addadminuser(AddAdminUser addAdminUser) throws SQLException{
			
			int userRoleId     = 0;
			boolean sendMail = false;
			
			Connection conn = DatabaseConnection.getConnection();
				
			String generation_Password = PasswordEncryption.generateAccountPassword();
			String account_password =PasswordEncryption.encryption(generation_Password);
				
			sendMail = SendMail.sendingmail(account_password, addAdminUser.getMail(), addAdminUser.getName());
				
			if (sendMail) {
				
				PreparedStatement FetchMaxAccountIdUserRole =conn.prepareStatement(ConstantValues.FETCH_MAX_USERROLE_ID);
				ResultSet FetchUserRoleResult = FetchMaxAccountIdUserRole.executeQuery();
				
				while (FetchUserRoleResult.next()) {
					
					userRoleId = FetchUserRoleResult.getInt("maxId");
					
				}
				
				if (userRoleId == 0) {
					userRoleId = userRoleId+1010;
				}else{
					userRoleId = userRoleId+1;
			    }
				  
				PreparedStatement InsertIntoUserRole = conn.prepareStatement(ConstantValues.INSERT_INTO_USERROLE);
			    InsertIntoUserRole.setString(1,addAdminUser.getMail());
			    InsertIntoUserRole.setString(2,account_password);
			    InsertIntoUserRole.setString(3,ConstantValues.ROLE_ADMIN);
			    InsertIntoUserRole.executeUpdate();
			       
			    PreparedStatement InsertIntoAdminAccountDetails = conn.prepareStatement(ConstantValues.INSERT_INTO_ADMIN_DETAILS);
			    InsertIntoAdminAccountDetails.setInt(1,userRoleId);
			    InsertIntoAdminAccountDetails.setString(2,account_password);
			    InsertIntoAdminAccountDetails.setString(3,addAdminUser.getName());
			    InsertIntoAdminAccountDetails.setString(4,addAdminUser.getMail());
			    InsertIntoAdminAccountDetails.setString(5,addAdminUser.getPhone_no());
			    InsertIntoAdminAccountDetails.setString(6,addAdminUser.getTravels_name());
			    InsertIntoAdminAccountDetails.executeUpdate();
			    
			    
			    PreparedStatement InsertIntoAdminBankDetails = conn.prepareStatement("insert into adminbank_details(bank_name,account_number,card_number,card_exp,admin_id)values(?,?,?,?,?)");
			    InsertIntoAdminBankDetails.setString(1,addAdminUser.getBankName());
			    InsertIntoAdminBankDetails.setString(2,addAdminUser.getAccountNumber());
			    InsertIntoAdminBankDetails.setString(3,addAdminUser.getCardNumber());
			    InsertIntoAdminBankDetails.setString(4,addAdminUser.getExpiryDate());
			    InsertIntoAdminBankDetails.setInt(5,userRoleId);
			    InsertIntoAdminBankDetails.executeUpdate();
			    
			    return ConstantValues.SUCCESS;
					
				}else{
					
					return "WRONG_MAIL";
				}
			
		}
		
		public boolean UserNameAvailable(String username) throws SQLException{
			
			boolean returnValue;
			Connection conn = DatabaseConnection.getConnection();
			
			PreparedStatement UserNameAvailableCheck =conn.prepareStatement(ConstantValues.USERNAME_AVAILABLE_CHECK);
			UserNameAvailableCheck.setString(1, username);
			ResultSet UserNameAvailableResult = UserNameAvailableCheck.executeQuery();
			
			returnValue = UserNameAvailableResult.next();
			
			if (returnValue) {
				return false;
			}else{
				return true;
			}
				
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		public String getTravels_name() {
			return travels_name;
		}
		public void setTravels_name(String travels_name) {
			this.travels_name = travels_name;
		}
		public String getPhone_no() {
			return phone_no;
		}
		public void setPhone_no(String phone_no) {
			this.phone_no = phone_no;
		}
		public String getBankName() {
			return bankName;
		}
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
		public String getAccountNumber() {
			return accountNumber;
		}
		public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
		}
		public String getCardNumber() {
			return cardNumber;
		}
		public void setCardNumber(String cardNumber) {
			this.cardNumber = cardNumber;
		}
		public String getExpiryDate() {
			return expiryDate;
		}
		public void setExpiryDate(String expiryDate) {
			this.expiryDate = expiryDate;
		}
		
		
	
}
