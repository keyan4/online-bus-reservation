package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
	
	
    public static Connection conn =null;
	 

	private DatabaseConnection(){
		
	}
	
	public static Connection getConnection(){  
		
		try {
			
			if (conn == null) {
				
				
				Class.forName("com.mysql.cj.jdbc.Driver");  
				conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/bus", "root", "");  
				
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return conn;
	}
			
}
