package utility;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public class SendMail {
	
	
	public static boolean sendingmail(String account_password,String gmail,String name){
        final String username = "customizedlearningforzoho@gmail.com";
        final String password = "*************";
        String greetings="";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH");
        LocalDateTime now = LocalDateTime.now();
        String timeinString= dtf.format(now);
        int time=Integer.parseInt(timeinString);

        if (time>=8 && time<12){
            greetings="Good Morning";
        }else if(time>=12 && time<16){
            greetings="Good AfterNoon";
        }else if (time>=16){
            greetings="Good Evening";
        }

        Properties props = new Properties();

        props.put("mail.smtp.starttls.enable", "true");

        props.put("mail.smtp.auth", "true");

        props.put("mail.smtp.host", "smtp.gmail.com");

        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
        	
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });


        try {


            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress("customizedlearningforzoho@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(gmail));
            message.setSubject("Password for your account");
            message.setText(greetings+" "+name+",\n\n your password number is \uD83D\uDCEC : "+account_password);
            try {
                Transport.send(message);
                System.out.println("your otp is send to your \uD83D\uDCEC "+gmail);
            }catch(Exception ex){
                System.out.println("please enter the correct G-mail id....");
                return false;
            }

            return true;
        } catch (MessagingException e) {
            System.out.println("mail not send...");
            return false;
        }


    }

}
