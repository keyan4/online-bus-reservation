package utility;

public class AddTripConstructor {
	
	String seat_number;
	int trip_id;
	
	
	public String getSeat_number() {
		return seat_number;
	}
	public void setSeat_number(String seat_number) {
		this.seat_number = seat_number;
	}
	public int getTrip_id() {
		return trip_id;
	}
	public void setTrip_id(int trip_id) {
		this.trip_id = trip_id;
	}
	
	public AddTripConstructor(String seat_number,int trip_id){
		this.seat_number = seat_number;
		this.trip_id     = trip_id;
	}

}
