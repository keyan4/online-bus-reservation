package utility;

public class ConstantValues {
	
	public enum USERROLES { ADMIN, PASSANGER}  
	
	public static final String LOG_INCHECK                     = "select * from userrole where username =? and password =?";
	
	public static final String FETCH_MAX_USERROLE_ID           = "SELECT max(account_id) as maxId FROM userrole";
	
	public static final String FETCH_MAX_TRIP_ID               = "SELECT max(trip_id) as maxId FROM trip_details";
	
	public static final String INSERT_INTO_USERROLE            = "insert into userrole (username,password,role)values(?,?,?)";
	
	public static final String INSERT_INTO_ADMIN_DETAILS       = "insert into adminaccount_details(admin_id,password,name,mail,phone_no,travels_name)values(?,?,?,?,?,?)"; 
	
	public static final String INSERT_INTO_PASSANGER_DETAILS   = "insert into passangeraccount_details(passanger_id,password,name,mail,phone_no)values(?,?,?,?,?)"; 
	
	public static final String USERNAME_AVAILABLE_CHECK        = "select * from userrole where username =?";
	
	public static final String ROLE_ADMIN                      = "ADMIN";
	
	public static final String ROLE_PASSANGER                  = "PASSANGER";
	
	public static final String ADMIN_SINGLE_TABLE              = "adminS";
	
	public static final String ADMIN_MULTIPLE_TABLE            = "adminM";
	
	public static final String PASSANGER_TICKET_TABLE          = "passangerT";
	
	public static final String PASSANGER_BANK_TABLE            = "passangerB";
	
	public static final String STATUS                          = "STATUS";
	
	public static final String SUCCESS                         = "SUCCESS";
	
	public static final String FAILURE                         = "FAILURE";
	
	public static final String VALUE_NULL                      = "VALUE_NULL";
	
	public static final String TRIP_NAME_INCORRECT             = "TRIP_NAME_INCORRECT";
	
	public static final String FROM_PLACE_INCORRECT            = "FROM_PLACE_INCORRECT";
	
	public static final String TO_PLACE_INCORRECT              = "TO_PLACE_INCORRECT";
	
	public static final String DATE_INCORRECT                  = "DATE_INCORRECT";
	
	public static final String ARRIVAL_INCORRECT               = "ARRIVAL_INCORRECT";
	
	public static final String DEPARTURE_INCORRECT             = "DEPARTURE_INCORRECT";
	
	public static final String SEATCOUNT1_INCORRECT            = "SEATCOUNT1_INCORRECT";
	
	public static final String SEATCOUNT2_INCORRECT            = "SEATCOUNT2_INCORRECT";
	
	public static final String SEATTYPE1_INCORRECT             = "SEATTYPE1_INCORRECT";
	
	public static final String SEATTYPE2_INCORRECT             = "SEATTYPE2_INCORRECT";
	
	public static final String ACTYPE_INCORRECT                = "ACTYPE_INCORRECT";
	
	public static final String RATE1_INCORRECT                 = "RATE1_INCORRECT";
	
	public static final String RATE2_INCORRECT                 = "RATE2_INCORRECT";
	
	public static final String BUS_NUMBER_INCORRECT            = "BUS_NUMBER_INCORRECT";
	
	public static final String TRAVELS_NAME_INCORRECT          = "TRAVELS_NAME_INCORRECT";
	
	public static final String BUSNUMBER_EXIST                 = "BUSNUMBER_EXIST";
	
	public static final String DATE_FROMAT                     = "yyyy-MM-dd";
	
	public static final String FROM_PLACE                      = "from_place";
	
	public static final String TO_PLACE                        = "TO_PLACE";
	
	public static final String NOT_AVAILABLE                   = "NOT_AVAILABLE";
	
	public static final String AVAILABLE                       = "AVAILABLE";

	public static final String INSERT_INTO_BUSDETAILS          = "insert into bus_details (bus_number,travels_name,admin_id)values(?,?,?)";
	
	public static final String BUSNUMBER_ALREADY_EXIST_CHECK   = "select * from bus_details where bus_number = ?";
	
	public static final String FETCH_BUSID_FORM_BUS_DETAILS    = "select * from bus_details where bus_number = ?";
	
	public static final String FETCH_QUERY_FROM_PLACE          = "select * from trip_details where date = ?";
	
	public static final String FETCH_QUERY_TO_PLACE            = "select * from trip_details where date = ?";
	
	public static final String INSERT_INTO_SINGLE_TRIP_DETAILS = "insert into trip_details (trip_name,from_place,to_place,arrival_time,departure_time,seat_count1,seat_type1,ac_type,rate1,bus_id,date)values(?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String INSERT_INTO_MULTIPLE_TRIP_DETAILS = "insert into trip_details (trip_name,from_place,to_place,arrival_time,departure_time,seat_count1,seat_type1,ac_type,rate1,bus_id,date,seat_count2,seat_type2,rate2)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String INSERT_INTO_SEAT_DETAILS         = "insert into seat_details (seat_number,trip_id,status)values(?,?,?)";
	
	public static final String FETCH_DATE_DETAILS_TRIP_DETAILS = "select * from trip_details where bus_id = ?";
	
	public static final String FETCH_DATA_BUS_SEARCH            = "select * from trip_details inner join bus_details on trip_details.bus_id = bus_details.bus_id  where date = ? and from_place = ? and to_place = ?";
	
	public static final String TICKET_BOOKED_COMMONPASSANGER    = "update seat_details set name = ?, mail = ?, phone_no = ?, gender = ?, status = ?  where trip_id = ? and seat_number =?";
	
	public static final String TICKET_BOOKED_PASSANGER          = "update seat_details set name = ?, mail = ?, phone_no = ?, gender = ?, status = ?, passanger_id = ?  where trip_id = ? and seat_number =?";
	
	public static final String INSERT_INTO_PASSANGERBANK        = "insert into passangerbank_details (card_number,card_exp,passanger_id)values(?,?,?)";
}
