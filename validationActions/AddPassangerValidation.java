package validationActions;

import java.util.HashMap;

import ownerActions.AddAdminUser;
import ownerActions.CreateAccountPassanger;
import utility.ConstantValues;

public class AddPassangerValidation {
	
	
	
	public HashMap<String, String> passangerValditaion(CreateAccountPassanger createAccountPassanger){
		
		HashMap<String, String> validation = new HashMap<String, String>();
			
			if (mailValidation(createAccountPassanger.getMail())) {
				
				if (phonenovalidation(createAccountPassanger.getPhone_no())) {
					
					if (NameValidation(createAccountPassanger.getName())) {
						validation.put(ConstantValues.STATUS, "SUCCESS");
					}else{
						validation.put(ConstantValues.STATUS, "NAME_INCORRECT");
					}
				}else{
					validation.put(ConstantValues.STATUS, "PHONENO_INCORRECT");
				}
			}else{
				validation.put(ConstantValues.STATUS, "MAIL_INCORRECT");
			}
			
			return validation;
		}
		
		
		public boolean mailValidation(String toplace) {

			if (toplace.matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](gmail)[.]com$")) {
				return true;
			} else {
				return false;
			}

		}
		
		public boolean phonenovalidation(String phone) {

			if (phone.matches("[0-9]+")) {
				if (phone.length()<=15) {
					return true;
				}else{
					return false;
				}
			} else {
				return false;
			}

		}
		
		public boolean NameValidation(String bus_name) {

			if (bus_name.matches("[a-zA-Z]+")) {
				return true;
			} else {
				return false;
			}

		}

	
}
