package validationActions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.regex.Pattern;

import ownerActions.AddAdminUser;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class AddAdminUserValidation {
	
	
	public HashMap<String, String> addminuserValditaion(AddAdminUser addAdminUser){
		
		HashMap<String, String> validation = new HashMap<String, String>();
		
		if (mailValidation(addAdminUser.getMail())) {
			
			if (phonenovalidation(addAdminUser.getPhone_no())) {
				
				if (NameValidation(addAdminUser.getName())) {
					
					if (travelsValidation(addAdminUser.getTravels_name())) {
						
						if (NameValidation(addAdminUser.getBankName())) {
							
							if (accountNumbervalidation(addAdminUser.getAccountNumber())) {
								
								if (cardNumbervalidation(addAdminUser.getCardNumber())) {
									
									if (expiryDateValidation(addAdminUser.getExpiryDate())) {
										validation.put("status", "SUCCESS");
									}else{
										validation.put("status", "EXPRIYDATE_INCORRECT");
									}
								}else{
									validation.put("status", "CARDNUMBER_INCORRECT");
								}
							}else{
								validation.put("status", "ACCOUNTNUMBER_INCORRECT");
							}
						}else{
							validation.put("status", "BANKNAME_INCORRECT");
						}
					}else{
						validation.put("status", "TRAVELSNAME_INCORRECT");
					}
				}else{
					validation.put("status", "NAME_INCORRECT");
				}
			}else{
				validation.put("status", "PHONENO_INCORRECT");
			}
		}else{
			validation.put("status", "MAIL_INCORRECT");
		}
		
		return validation;
	}
	
	
	public boolean mailValidation(String toplace) {

		if (toplace.matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](gmail)[.]com$")) {
			return true;
		} else {
			return false;
		}

	}
	
	public boolean phonenovalidation(String phone) {

		if (phone.matches("[0-9]+")) {
			if (phone.length()<=15 && phone.length()>=10) {
				return true;
			}else{
				return false;
			}
		} else {
			return false;
		}

	}
	
	public boolean NameValidation(String bus_name) {

		if (bus_name.matches("[a-zA-Z ]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean travelsValidation(String fromplace) {

		if (fromplace.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}

	}
	
	public boolean expiryDateValidation(String fromplace) {

		if (fromplace.matches("[0-9]{2}[-][0-9]{4}")) {
			return true;
		} else {
			return false;
		}

	}
	
	public boolean accountNumbervalidation(String accountNumber) {

		if (accountNumber.matches("[A-Za-z0-9]+")) {
			if (accountNumber.length()==16) {
				return true;
			}else{
				return false;
			}
		} else {
			return false;
		}

	}
	
	public boolean cardNumbervalidation(String cardNumber) {

		if (cardNumber.matches("[0-9]+")) {
			if (cardNumber.length()==16) {
				return true;
			}else{
				return false;
			}
		} else {
			return false;
		}

	}
	
	
	
	
	
	


}
