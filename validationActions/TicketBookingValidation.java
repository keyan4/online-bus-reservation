package validationActions;

import java.util.HashMap;

import commonPassangerActions.TicketBooked;
import utility.ConstantValues;

public class TicketBookingValidation {

	
	public HashMap<String, String> passangerticketBooking(passangerActions.TicketBooked ticketBooked){
		
		
		HashMap<String, String> validation = new HashMap<String, String>();
		
		if (!ticketBooked.getTrip_id().isEmpty() && !ticketBooked.getName().isEmpty()  && !ticketBooked.getMail().isEmpty()  && !ticketBooked.getPhone_no().isEmpty()
				 && !ticketBooked.getGender().isEmpty()  && !ticketBooked.getCard_number().isEmpty()  && !ticketBooked.getCcv_number().isEmpty()  && !ticketBooked.getSave_card().isEmpty()) {
			
			if (busNumberValidation(ticketBooked.getTrip_id())) {
				
				if (NameValidation(ticketBooked.getName())) {
					
					if (mailValidation(ticketBooked.getMail())) {
						
						if (phonenovalidation(ticketBooked.getPhone_no())) {
							
							if (genderValidation(ticketBooked.getGender())) {
								
								if (cardnumberValidation(ticketBooked.getCard_number())) {
									
									if (ccvValidation(ticketBooked.getCcv_number())) {
										validation.put(ConstantValues.STATUS, "SUCCESS");
									}else{
										validation.put(ConstantValues.STATUS, "CCVNUMBER");
									}
								}else{
									validation.put(ConstantValues.STATUS, "CARDNUMBER");
								}
							}else{
								validation.put(ConstantValues.STATUS, "GENDER");
							}
						}else{
							validation.put(ConstantValues.STATUS, "PHONENO");
						}
					}else{
						validation.put(ConstantValues.STATUS, "MAIL");
					}
				}else{
					validation.put(ConstantValues.STATUS, "NAME");
				}
			}else{
				validation.put(ConstantValues.STATUS, "NAME");
			}
		}else{
			validation.put(ConstantValues.STATUS, ConstantValues.VALUE_NULL);
		}
		
		
		
		
		
		return validation;
	}
	
	
public HashMap<String, String> commonpassangerticketBooking(commonPassangerActions.TicketBooked ticketBooked){
		
		
		HashMap<String, String> validation = new HashMap<String, String>();
		
		if (!ticketBooked.getTrip_id().isEmpty() && !ticketBooked.getName().isEmpty()  && !ticketBooked.getMail().isEmpty()  && !ticketBooked.getPhone_no().isEmpty()
				 && !ticketBooked.getGender().isEmpty()  && !ticketBooked.getCard_number().isEmpty()  && !ticketBooked.getCcv_number().isEmpty()) {
			
			if (busNumberValidation(ticketBooked.getTrip_id())) {
				
				if (NameValidation(ticketBooked.getName())) {
					
					if (mailValidation(ticketBooked.getMail())) {
						
						if (phonenovalidation(ticketBooked.getPhone_no())) {
							
							if (genderValidation(ticketBooked.getGender())) {
								
								if (cardnumberValidation(ticketBooked.getCard_number())) {
									
									if (ccvValidation(ticketBooked.getCcv_number())) {
										validation.put(ConstantValues.STATUS, "SUCCESS");
									}else{
										validation.put(ConstantValues.STATUS, "CCVNUMBER");
									}
								}else{
									validation.put(ConstantValues.STATUS, "CARDNUMBER");
								}
							}else{
								validation.put(ConstantValues.STATUS, "GENDER");
							}
						}else{
							validation.put(ConstantValues.STATUS, "PHONENO");
						}
					}else{
						validation.put(ConstantValues.STATUS, "MAIL");
					}
				}else{
					validation.put(ConstantValues.STATUS, "NAME");
				}
			}else{
				validation.put(ConstantValues.STATUS, "NAME");
			}
		}else{
			validation.put(ConstantValues.STATUS, ConstantValues.VALUE_NULL);
		}
			
		
		
		
		
		
		return validation;
	}
	
	
	public boolean NameValidation(String bus_name) {

		if (bus_name.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean genderValidation(String fromplace) {

		if (fromplace.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean mailValidation(String toplace) {

		if (toplace.matches("^[!#$%&'*\\/=?^_+-`{|}~a-zA-Z0-9]{6,30}[@](gmail)[.]com$")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean busNumberValidation(String bus_number) {

		if (bus_number.matches("[0-9]+")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean phonenovalidation(String phone) {

		if (phone.matches("[0-9]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean cardnumberValidation(String cardnumber) {

		if (cardnumber.matches("[0-9]+")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean ccvValidation(String ccv) {

		if (ccv.matches("[0-9]+")) {
			return true;
		} else {
			return false;

		}
	}
}
