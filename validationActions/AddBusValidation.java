package validationActions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import adminActions.AddBus;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class AddBusValidation {

	public HashMap<String, String> addBusValidation(AddBus addBus) {

		HashMap<String, String> validation = new HashMap<String, String>();
		
		if (!addBus.getBus_number().isEmpty() && !addBus.getTravels_name().isEmpty()) {
			
			if (busNumberValidation(addBus.getBus_number())) {
				
				if (travelsNameValidation(addBus.getTravels_name())) {
					
					if (BusNumberAlreadyExistCheck(addBus.getBus_number())) {
						validation.put(ConstantValues.STATUS, ConstantValues.SUCCESS);
					}else{
						validation.put(ConstantValues.STATUS, ConstantValues.BUSNUMBER_EXIST);
					}
				}else{
					validation.put(ConstantValues.STATUS, ConstantValues.TRAVELS_NAME_INCORRECT);
				}
			}else{
				validation.put(ConstantValues.STATUS, ConstantValues.BUS_NUMBER_INCORRECT);
			}
		}else{
			validation.put(ConstantValues.STATUS, ConstantValues.VALUE_NULL);
		}
		
		return validation;
		
	}
	
	public boolean busNumberValidation(String bus_number) {

		if (bus_number.matches("[a-zA-Z0-9 ]+")) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean travelsNameValidation(String fromplace) {

		if (fromplace.matches("[a-zA-Z ]+")) {
			return true;
		} else {
			return false;
		}

	}
	
	
	public boolean BusNumberAlreadyExistCheck(String busNumber){
		
		try {
			
			Connection dataConnection = DatabaseConnection.getConnection();
			
			PreparedStatement BusNumberAlreadyExistCheck = dataConnection.prepareStatement(ConstantValues.BUSNUMBER_ALREADY_EXIST_CHECK);
			BusNumberAlreadyExistCheck.setString(1, busNumber);
			ResultSet BusNumberAlreadyExistCheckResult = BusNumberAlreadyExistCheck.executeQuery();
			
			boolean returnValue = BusNumberAlreadyExistCheckResult.next();
			
			if (returnValue) {
				return false;
			}else{
				return true;
			}
			
		} catch (Exception e) {
			
			return false;
		}
	}
}
