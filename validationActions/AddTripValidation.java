package validationActions;

import java.util.HashMap;

import adminActions.AddTripMultiple;
import adminActions.AddTripSingle;
import utility.ConstantValues;

public class AddTripValidation {

	public HashMap<String, String> addTripSingleValdiation(AddTripSingle addTrip) {

		HashMap<String, String> validation = new HashMap<String, String>();
		
		if (!addTrip.getBus_number().isEmpty() && !addTrip.getTrip_name().isEmpty() && !addTrip.getFrom_place().isEmpty() && !addTrip.getTo_place().isEmpty() 
				&& !addTrip.getDate().isEmpty() && !addTrip.getArrival_time().isEmpty() && !addTrip.getDeparture_time().isEmpty() && !addTrip.getSeat_count1().isEmpty() 
				&& !addTrip.getSeat_type1().isEmpty() && !addTrip.getAc_type().isEmpty() && !addTrip.getRate1().isEmpty()) {
			
			if (busNumberValidation(addTrip.getBus_number())) {
				
				if (TripNameValidation(addTrip.getTrip_name())) {
					
					if (fromplacValidation(addTrip.getFrom_place())) {
						
						if (toplaceValidation(addTrip.getTo_place())) {
							
							if (dateValidation(addTrip.getDate())) {
								
								if (arrival_depature_timeValidation(addTrip.getArrival_time())) {
									
									if (arrival_depature_timeValidation(addTrip.getDeparture_time())) {
										
										if (seatCountValidation(addTrip.getSeat_count1())) {
											
											if (seatTypeIdvalidation(addTrip.getSeat_type1())) {
												
												if (Actypevalidation(addTrip.getAc_type())) {
													
													if (rateValidation(addTrip.getRate1())) {
														validation.put(ConstantValues.STATUS, ConstantValues.SUCCESS);
													}else{
														validation.put(ConstantValues.STATUS, ConstantValues.RATE1_INCORRECT);
													}
												}else{
													validation.put(ConstantValues.STATUS, ConstantValues.ACTYPE_INCORRECT);
												}
											}else{
												validation.put(ConstantValues.STATUS, ConstantValues.SEATTYPE1_INCORRECT);
											}
										}else{
											validation.put(ConstantValues.STATUS, ConstantValues.SEATCOUNT1_INCORRECT);
										}
									}else{
										validation.put(ConstantValues.STATUS, ConstantValues.DEPARTURE_INCORRECT);
									}
								}else{
									validation.put(ConstantValues.STATUS, ConstantValues.ARRIVAL_INCORRECT);
								}
							}else{
								validation.put(ConstantValues.STATUS, ConstantValues.DATE_INCORRECT);
							}
						}else{
							validation.put(ConstantValues.STATUS, ConstantValues.TO_PLACE_INCORRECT);
						}
					}else{
						validation.put(ConstantValues.STATUS, ConstantValues.FROM_PLACE_INCORRECT);
					}
				}else{
					validation.put(ConstantValues.STATUS, ConstantValues.TRIP_NAME_INCORRECT);
				}
			}else{
				validation.put(ConstantValues.STATUS, ConstantValues.BUS_NUMBER_INCORRECT);
			}	 
		}else{
			validation.put(ConstantValues.STATUS, ConstantValues.VALUE_NULL);
		}
		
		
		return validation;
		
	}
	
	public HashMap<String, String> addTripMultipleValdiation(AddTripMultiple addTrip) {

		HashMap<String, String> validation = new HashMap<String, String>();
		
		if (!addTrip.getBus_number().isEmpty() && !addTrip.getTrip_name().isEmpty() && !addTrip.getFrom_place().isEmpty() && !addTrip.getTo_place().isEmpty() 
				&& !addTrip.getDate().isEmpty() && !addTrip.getArrival_time().isEmpty() && !addTrip.getDeparture_time().isEmpty() && !addTrip.getSeat_count1().isEmpty() 
				&& !addTrip.getSeat_type1().isEmpty() && !addTrip.getAc_type().isEmpty() && !addTrip.getRate1().isEmpty() && !addTrip.getSeat_count2().isEmpty()
				&& !addTrip.getSeat_type2().isEmpty() && !addTrip.getRate2().isEmpty()) {
			
			if (busNumberValidation(addTrip.getBus_number())) {
				
				if (TripNameValidation(addTrip.getTrip_name())) {
					
					if (fromplacValidation(addTrip.getFrom_place())) {
						
						if (toplaceValidation(addTrip.getTo_place())) {
							
							if (dateValidation(addTrip.getDate())) {
								
								if (arrival_depature_timeValidation(addTrip.getArrival_time())) {
									
									if (arrival_depature_timeValidation(addTrip.getDeparture_time())) {
										
										if (seatCountValidation(addTrip.getSeat_count1())) {
											
											if (seatTypeIdvalidation(addTrip.getSeat_type1())) {
												
												if (Actypevalidation(addTrip.getAc_type())) {
													
													if (rateValidation(addTrip.getRate1())) {
														
														if (seatCountValidation(addTrip.getSeat_count2())) {
															
															if (seatTypeIdvalidation(addTrip.getSeat_type2())) {
																
																if (rateValidation(addTrip.getRate2())) {
																	validation.put(ConstantValues.STATUS, ConstantValues.SUCCESS);
																}else{
																	validation.put(ConstantValues.STATUS, ConstantValues.RATE2_INCORRECT);
																}
															}else{
																validation.put(ConstantValues.STATUS, ConstantValues.SEATTYPE2_INCORRECT);
															}
														}else{
															validation.put(ConstantValues.STATUS, ConstantValues.SEATCOUNT2_INCORRECT);
														}
													}else{
														validation.put(ConstantValues.STATUS, ConstantValues.RATE1_INCORRECT);
													}
												}else{
													validation.put(ConstantValues.STATUS, ConstantValues.ACTYPE_INCORRECT);
												}
											}else{
												validation.put(ConstantValues.STATUS, ConstantValues.SEATTYPE1_INCORRECT);
											}
										}else{
											validation.put(ConstantValues.STATUS, ConstantValues.SEATCOUNT1_INCORRECT);
										}
									}else{
										validation.put(ConstantValues.STATUS, ConstantValues.DEPARTURE_INCORRECT);
									}
								}else{
									validation.put(ConstantValues.STATUS, ConstantValues.ARRIVAL_INCORRECT);
								}
							}else{
								validation.put(ConstantValues.STATUS, ConstantValues.DATE_INCORRECT);
							}
						}else{
							validation.put(ConstantValues.STATUS, ConstantValues.TO_PLACE_INCORRECT);
						}
					}else{
						validation.put(ConstantValues.STATUS, ConstantValues.FROM_PLACE_INCORRECT);
					}
				}else{
					validation.put(ConstantValues.STATUS, ConstantValues.TRIP_NAME_INCORRECT);
				}
			}else{
				validation.put(ConstantValues.STATUS, ConstantValues.BUS_NUMBER_INCORRECT);
			}	 
		}else{
			validation.put(ConstantValues.STATUS, ConstantValues.VALUE_NULL);
		}
		
		
		return validation;
		
	}
	
	
	public boolean TripNameValidation(String bus_name) {

		if (bus_name.matches("[a-zA-Z0-9]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean fromplacValidation(String fromplace) {

		if (fromplace.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean toplaceValidation(String toplace) {

		if (toplace.matches("[a-zA-Z]+")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean busNumberValidation(String bus_number) {

		if (bus_number.matches("[a-zA-Z0-9 ]+")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean dateValidation(String date) {

		if (date.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")) {
			return true;
		} else {
			return false;
		}

	}

	public boolean arrival_depature_timeValidation(String arrival_time) {

		if (arrival_time.matches("[0-9]{2}:[0-9]{2}")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean seatCountValidation(String Seatcount) {

		if (Seatcount.matches("[0-9]+")) {
			return true;
		} else {
			return false;

		}
	}

	public boolean rateValidation(String rate) {

		if (rate.matches("[0-9]+")) {
			return true;
		} else {
			return false;

		}
	}

	public boolean seatTypeIdvalidation(String SeatType) {
		if (SeatType.matches("[a-zA-Z ]+")) {
			return true;
		} else {
			return false;
		}
	}

	public boolean Actypevalidation(String actype) {
		if (actype.matches("[a-zA-Z ]+")) {
			return true;
		} else {
			return false;
		}
	}
}
