<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
    .D110_main_upper_container{
        background-color: #d84f57;
        display: inline-block;
        width: 1685px;
        height: 50px;
        margin-top: -10px;
        margin-left: -10px;
    }
    .D110_upper_divcontainer{
        display: inline-block;
        margin-left: 1500px;
        margin-top: 10px;
    }
    .D111_main_mid_container{
        background-image: url("./Img/redbusimg.png");
        width: 1685px;
        height: 580px;
        margin-left: -10px;
        display: block;
    }
    .D111_input_divcontainer{
        display: inline-block;
        margin-left: 300px;
        margin-top: 280px;
    }
    .D111_input_valuecontainer{
        padding: 15px;
        display: inline-block;
        margin-top: 20px;
        width: 260px;
    }
    .D110_signin_up_button {
        padding: 10px;
        width: 120px;
        border-bottom: 1px solid #d84f57;
        border-top: 1px solid #d84f57;
        border-left: 1px solid #d84f57;
        border-right: 1px solid #d84f57;
        border-radius: 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 12px;
    }
    .D110_signin_up_button1 {
        background-color: white; 
        color: black; 
        border: 2px solid #d84f57;
    }
    .D110_signin_up_button1:hover {
        background-color: #d84f57;
        border: 2px solid #fff;
        color: white;
    }

    .D111_menuContainer{
        margin-top: 70px;
        width: 1400px;
        height: 250px;
        display: block;
       
    }

    .D111_menuButtonContainer{
        margin-left: 100px;
        width: 200px;
        height: 200px;
        display: inline-block;
        background-color: #FFFFFF;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
    }

    .D112_AddDivContainer{
        display: none;
        width: 1665px;
        height: 950px;
        background: url(./Img/background_1.jpg);
    }
    .D113_TicketBookedDivContainer{
        display: none;
        width: 1665px;
        height: 950px;
        background: url(./Img/background_1.jpg);
    }
    .D114_RemoveBusDivContainer{
        display: none;
        width: 1665px;
        height: 950px;
        background: url(./Img/background_1.jpg);
    }
    .D114_RemoveBusdiv{
       display: inline-block;
        width: 280px;
        height: 430px;
        margin-left: 100px;
        margin-top: 50px;
        box-shadow: inset 0px 0px 10px rgba(0,0,0,0.9);
        padding: 10px;
        float: left;
        overflow: scroll;
        background-color: #FFFFFF;
    }
    .D114_RemoveBusdiv1{
       display: inline-block;
        width: 380px;
        height: 430px;
        margin-left: 100px;
        margin-top: 50px;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
        padding: 10px;
        float: left;
        overflow: scroll;
        background-color: #FFFFFF;
    }
    .close {
        color: violet;
        float: right;
        margin-right: 15px;
        font-size: 28px;
        font-weight: bold;
    }
    .D112_SubAddDivContainer1{
        width: 500px;
        height: 700px;
        margin-top:120px;
        margin-left: 200px;
        background-color: #FFFFFF;
        display: block;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
        border-radius: 5px;
        padding: 10px;
        float: left;
    }
    .D112_SubAddDivContainer2{
        width: 500px;
        height: 700px;
        margin-top:120px;
        margin-left: 200px;
        background-color: #FFFFFF;
        display: inline-block;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
        border-radius: 5px;
        padding: 10px;
    }
    .D112_InputValueContainer{
        margin-top: 30px;
        width: 170px;
        padding: 10px;
        display: block;
        background-color: #FFFFFF;
        margin-left: 30px;
        border-radius: 5px;
    }

    .D112_SeatTypeButton{
        margin-top:120px;
        margin-left: 30px;
        display: inline-block;
        background-color: violet;
        color: #FFFFFF;
        font-size: 15px;
        width: 190px;
        border-radius: 5px;
        padding: 10px;

    }
    .D113_SubDivContainer1{
        display: inline-block;
        width: 280px;
        height: 430px;
        margin-left: 100px;
        margin-top: 50px;
        box-shadow: inset 0px 0px 10px rgba(0,0,0,0.9);
        padding: 10px;
        float: left;
        overflow: scroll;
        background-color: #FFFFFF;
    }
    .D113_SubDivContainer2{
        display: inline-block;
        width: 570px;
        height: 450px;
        margin-left: 30px;
        margin-top: 50px;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
        background-color: #FFFFFF;
    }
    .D113_unScrollDivContainer2{
        width: 570px;
        height: 50px;
        background-color: #FFFFFF;
        border-radius: 5px;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
    }
    .D113_ScrollDivContainer2{
        width: 570px;
        height: 400px;
        overflow: scroll;
    }
    .D113_SubDivContainer3{
        display: inline-block;
        width: 500px;
        height: 850px;
        margin-right: 100px;
        margin-top: 50px;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
        background-color: #FFFFFF;
        float: right;
    }
    .D113_SubDivContainer4{
        display: inline-block;
        width: 900px;
        height: 370px;
        margin-left: 100px;
        margin-top: 30px;
        box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
        background-color: #FFFFFF;
        float: left;
        overflow: scroll;
    }
    .D113_ShowBusNameList{
        padding: 10px;
        text-transform: capitalize;
        border-bottom: 1px solid black;
    }
    .D114_TicketBooked{
        width: 570px;
        height: 50px;
        background-color: white;

    }

    .bothgrid{
        margin-top: 50px;
        float: left;
        margin-left: 50px;
        width: 400px;
        height: 700px;
        background-color: white;
        display: inline-block;
        overflow: scroll;
    }
    .gridsize{
        float: left;
        margin-top: 75px;
        width: 200px;
        height: 900px;
        background-color: white;
        display: inline-block;
        
    }
    .grid-container {
        display: grid;
        grid-column-gap: 10px;
        grid-row-gap: 10px;
        grid-template-columns: auto auto;
        background-color: white;
        padding: 30px;
    }


    .modal {
        display: none;
        position: fixed;
        z-index: 1;
        padding-top: 100px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0,0.4);
    }

    .modal-content {
        background-color: #fff;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        margin-top: 200px;
        width: 400px;
    }
    .modal-text{
        margin-left: 70px;
        font-size: 20px;
        width: 400px;
    }
    .model-button{
        margin-left: 150px;
        width: 100px;
        padding: 5px;
    }

    .close {
        color: dodgerblue;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
</style>
</head>
<body onload="allfunction()">

    <div id="SearchBus" style="display: none;">
        <div class="D110_main_upper_container" id="D110_main_upper_container">
            <div class="D110_upper_divcontainer">
                <h3><a href="logout">Logout</a></h3>
            </div>
        </div>
        <div class="D111_main_mid_container" id="D111_main_mid_container">
            <div class="D111_input_divcontainer" id="D111_input_divcontainer">
                <input class="D111_input_valuecontainer" list="from_place_datalist" name="from_place" placeholder="From Place" id="from_place_id" autocomplete="of">
                <datalist id="from_place_datalist">
                </datalist>
                <input class="D111_input_valuecontainer" list="to_place_datalist" name="to_place" placeholder="To Place" id="to_place_id" autocomplete="of">
                <datalist id="to_place_datalist">
                </datalist>
                <input class="D111_input_valuecontainer" type="date" name="date_time" id="date_time" placeholder="Date">
                <button class="D111_input_valuecontainer" id="search_bus" style="background-color: #d84f57" onclick="BusSearchCommon()">Search Bus</button>
            </div>
        </div>
        <div class="D111_menuContainer">
            <button class="D111_menuButtonContainer" id="AddBusButton" onclick="AddBusDiv()"></button>
            <button class="D111_menuButtonContainer" id="TicketbookedButton" onclick="TicketbookedDiv()"></button>
            <button class="D111_menuButtonContainer" id="BusListButton" onclick="RemoveBusDiv()"></button>
        </div>
        
    </div>
    <div class="D112_AddDivContainer" id="AddBusDiv">
        <span class="close" onclick="backfunction()">&times;</span>
        <div class="D112_SubAddDivContainer1">
            <input class="D112_InputValueContainer" type="text" name="bus_name" id="bus_name" placeholder="Bus Name" style="margin-top: 50px;"autocomplete="of"> 
            <input class="D112_InputValueContainer" type="text" name="bus_number" id ="bus_number" placeholder="Bus Number" autocomplete="of">
            <input class="D112_InputValueContainer" type="text" name="from_place" id="from_place" placeholder="From" autocomplete="of">
            <input class="D112_InputValueContainer" type="text" name="to_place" id="to_place" placeholder="To" autocomplete="of">
            <input class="D112_InputValueContainer" type="date" name="date" id="date" placeholder="Date">
            <input class="D112_InputValueContainer" type="time" name="arrival_time" id="arrival_time" placeholder="Arrival Time">
            <input class="D112_InputValueContainer" type="time" name="departure_time" id="departure_time" placeholder="Departure Time">
            <input class="D112_InputValueContainer" list="bus_type_datalist" placeholder="Bus Type Select" id="bus_type_id">
            <datalist id="bus_type_datalist">
                <option value="Single Type Bus">
                <option value="Multiple Type Bus">
            </datalist>
            <button class="D112_SeatTypeButton" style="margin-top: 40px;" onclick="busTypeSelect()">Bus Type</button>
        </div>
        <div class="D112_SubAddDivContainer2">
            <div id="single_type">
                <input class="D112_InputValueContainer" list="ac_type_datalist" placeholder="Ac Type" id="ac_type_id">
                <datalist id="ac_type_datalist">
                    <option value="Ac">
                    <option value="Non Ac">
                </datalist>
               
                <input class="D112_InputValueContainer" list="seat_type_datalist" placeholder="Seat Type" id="seat_type_id">
                <datalist id="seat_type_datalist">
                    <option value="Seater">
                    <option value="Semi Seater">
                    <option value="Sleeper">
                </datalist>
                <input class="D112_InputValueContainer" type="text" name="seat_count" id="seat_count" placeholder="Seat Count" autocomplete="of">
                <input class="D112_InputValueContainer" type="text" name="rate" id="rate" placeholder="Rate" autocomplete="of">
                <button class="D112_SeatTypeButton" style="margin-top: 350px; margin-left: 150px;" id="register_bus" onclick="AddBusFunctionsingle()">Register Bus</button>
            </div>
            <div id="multi_type" style="display: none;">
                 <input class="D112_InputValueContainer" list="ac_type_datalist" placeholder="Ac Type" id="ac_type_id1">
                <datalist id="ac_type_datalist">
                    <option value="Ac">
                    <option value="Non Ac">
                </datalist>
               
                <input class="D112_InputValueContainer" list="seat_type_datalist1" placeholder="Seat Type 1 " id="seat_type_id1">
                <datalist id="seat_type_datalist1">
                    <option value="Semi Seater">
                    <option value="Sleeper">
                </datalist>

                <input class="D112_InputValueContainer" list="seat_type_datalist2" placeholder="Seat Type 2" id="seat_type_id2">
                <datalist id="seat_type_datalist2">
                    <option value="Semi Seater">
                    <option value="Sleeper">
                </datalist>

                <input class="D112_InputValueContainer" type="text" name="seat_count1" id="seat_count1" placeholder="Seat Count 1" autocomplete="of">
                 <input class="D112_InputValueContainer" type="text" name="seat_count2" id="seat_count2" placeholder="Seat Count 2" autocomplete="of">

                <input class="D112_InputValueContainer" type="text" name="rate1" id="rate1" placeholder="Rate 1" autocomplete="of">

                 <input class="D112_InputValueContainer" type="text" name="rate2" id="rate2" placeholder="Rate 2" autocomplete="of">

                <button class="D112_SeatTypeButton" style="margin-top: 150px; margin-left: 150px;" id="register_bus" onclick="AddBusFunctionmultiple()">Register Bus</button>
            </div>
            
        </div>
    </div>
    <div class="D113_TicketBookedDivContainer" id="TicketbookedDiv">
        <span class="close" onclick="backfunction()">&times;</span>
        <div class="D113_SubDivContainer1" id="showBusNameList"></div>
        <div class="D113_SubDivContainer2">
            <div class="D113_unScrollDivContainer2">
                <input type="date" name="fetchbusbydate" id="fetchbusbydate">
                <button onclick="showFetchBusByDate()">Submit</button>
            </div>
            <div class="D113_ScrollDivContainer2" id="showfetchbusbydate"> </div>
        </div>
        <div class="D113_SubDivContainer3">
            <div class="bothgrid" id="SeatDiv">
            <div class="gridsize" >
                <div class="grid-container" id="seatCountLeft"></div>
            </div>
            <div class="gridsize">
                <div class="grid-container" id="seatCountRight"></div>
            </div>  
        </div>
        </div>
        <div class="D113_SubDivContainer4" id="FetchBookedTickets"></div>
    </div>
    <div class="D114_RemoveBusDivContainer" id="RemoveBusDiv">
        <span class="close" onclick="backfunction()">&times;</span>
        <div class="D114_RemoveBusdiv" id="removeBusList"></div>
        <div class="D114_RemoveBusdiv1">
            <input class="D112_InputValueContainer" type="text" name="bus_name" id="removebus_name" placeholder="Bus Name">
            <input class="D112_InputValueContainer" type="text" name="bus_number" id="removebus_number" placeholder="Bus Number">
            <button class="D112_SeatTypeButton" onclick="removebus()">Remove Bus</button>
        </div>
    </div>

    <div id="myModal" class="modal">
        <div class="modal-content">
            <p id="outputMsg" class="modal-text"></p>
            <button id="okbtn" class="model-button">ok</button>
        </div>
    </div>

    <script type="text/javascript">

        function AddBusDiv(){
            document.getElementById("AddBusDiv").style.display ="block";
            document.getElementById("SearchBus").style.display ="none";
            document.getElementById("TicketbookedDiv").style.display ="none";
            document.getElementById("RemoveBusDiv").style.display ="none";
            var AddBusDiv = "AddBusDiv";
            urlappendingprocess(AddBusDiv)

        }

        function AddBusDivreload(){
            document.getElementById("AddBusDiv").style.display ="block";
            document.getElementById("SearchBus").style.display ="none";
            document.getElementById("TicketbookedDiv").style.display ="none";
            document.getElementById("RemoveBusDiv").style.display ="none";
        }

        function TicketbookedDiv(){
            document.getElementById("TicketbookedDiv").style.display ="block";
            document.getElementById("SearchBus").style.display ="none";
            document.getElementById("AddBusDiv").style.display ="none";
            document.getElementById("RemoveBusDiv").style.display ="none";
            var TicketbookedDiv = "TicketbookedDiv";
            urlappendingprocess(TicketbookedDiv)
        }
        function TicketbookedDivreload(){
            document.getElementById("TicketbookedDiv").style.display ="block";
            document.getElementById("SearchBus").style.display ="none";
            document.getElementById("AddBusDiv").style.display ="none";
            document.getElementById("RemoveBusDiv").style.display ="none";
        }

        function RemoveBusDiv(){
            document.getElementById("RemoveBusDiv").style.display ="block";
            document.getElementById("SearchBus").style.display ="none";
            document.getElementById("AddBusDiv").style.display ="none";
            document.getElementById("TicketbookedDiv").style.display ="none";
            var RemoveBusDiv = "RemoveBusDiv";
            urlappendingprocess(RemoveBusDiv)
        }

        function RemoveBusDivreload(){
            document.getElementById("RemoveBusDiv").style.display ="block";
            document.getElementById("SearchBus").style.display ="none";
            document.getElementById("AddBusDiv").style.display ="none";
            document.getElementById("TicketbookedDiv").style.display ="none";
        }

        function backfunction(){
            document.getElementById("SearchBus").style.display ="block";
            document.getElementById("RemoveBusDiv").style.display ="none";
            document.getElementById("AddBusDiv").style.display ="none";
            document.getElementById("TicketbookedDiv").style.display ="none";
            backfunctionurlremoving()
        }

        function backfunctionurlremoving(){
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
            window.history.pushState({ path: newurl }, '', newurl);
        }

        function urlappendingprocess(div_id){
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?div_id='+div_id;
            window.history.pushState({ path: newurl }, '', newurl);
        
        }

        function BusSearchDiv(){
            document.getElementById("SearchBus").style.display ="block";
        }

        function busTypeSelect(){

            var bustype = document.getElementById("bus_type_id").value;

            console.log(bustype);

            if (bustype=="Single Type Bus") {
                document.getElementById("single_type").style.display ="block";
                document.getElementById("multi_type").style.display ="none";
            }else if (bustype=="Multiple Type Bus") {
                document.getElementById("multi_type").style.display ="block";
                document.getElementById("single_type").style.display ="none";
            }
        }

        function firstfunction(){
            var url_string =window.location.href;
            var url = new URL(url_string);
            var div_id = url.searchParams.get("div_id");

            console.log(div_id);

            return div_id;
        }
        
        function allfunction(){
            var div_id = firstfunction()
            if (div_id == "AddBusDiv") {
                AddBusDivreload()
            }else if (div_id == "TicketbookedDiv") {
                TicketbookedDivreload()
            }else if (div_id == "RemoveBusDiv") {
                RemoveBusDivreload()
            }else if (div_id==null) {
                 BusSearchDiv()
            }
            ShowFromPlace()
            ShowToPlace()
            showBusList()

        }

        function ShowFromPlace(){
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    ShowFromPlaceValue(this.responseText);
                }
            }
            xmlHttp.open("post", "ShowFromPlace"); 
            xmlHttp.send(); 
        }

        function ShowFromPlaceValue(from_place){
            var a=JSON.parse(from_place);
            var data = [];
            var list = document.getElementById('from_place_datalist');

            for (var i = 0; i < a.length; i++) {
                var from_place = a[i].from_place;
                data.push(from_place);
                
            }

            data.forEach(function(item){
                var option = document.createElement('option');
                option.value = item;
                list.appendChild(option);
            });
            
        }

        function ShowToPlace(){
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    ShowToPlaceValue(this.responseText);
                }
            }
            xmlHttp.open("post", "ShowToPlace"); 
            xmlHttp.send(); 
        }

        function ShowToPlaceValue(to_place){
            var a=JSON.parse(to_place);
            var data = [];
            var list = document.getElementById('to_place');

            for (var i = 0; i < a.length; i++) {
                var to_place = a[i].to_place;
                data.push(to_place);
                
            }

            data.forEach(function(item){
                var option = document.createElement('option');
                option.value = item;

                list.appendChild(option);
            });
            
        }


        function AddBusFunctionsingle(){

            var bus_name = encodeURIComponent(document.getElementById("bus_name").value);
            var bus_number = encodeURIComponent(document.getElementById("bus_number").value);
            var from_place = encodeURIComponent(document.getElementById("from_place").value);
            var to_place = encodeURIComponent(document.getElementById("to_place").value);
            var date = encodeURIComponent(document.getElementById("date").value);
            var arrival_time = encodeURIComponent(document.getElementById("arrival_time").value);
            var departure_time = encodeURIComponent(document.getElementById("departure_time").value);
            var seat_count = encodeURIComponent(document.getElementById("seat_count").value);
            var seat_type_id = encodeURIComponent(document.getElementById("seat_type_id").value);
            var ac_type_id = encodeURIComponent(document.getElementById("ac_type_id").value);
            var rate = encodeURIComponent(document.getElementById("rate").value);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    outputbox(this.responseText);
                }
            }
            xmlHttp.open("post", "AddSingleTypeBus?bus_name="+bus_name+"&bus_number="+bus_number+"&from_place="+from_place+"&to_place="+to_place+"&date="+date+"&arrival_time="+arrival_time+"&departure_time="+departure_time+"&seat_count="+seat_count+"&seat_type_id="+seat_type_id+"&ac_type_id="+ac_type_id+"&rate="+rate); 
            xmlHttp.send();
        }

        function AddBusFunctionmultiple(){

            var bus_name = encodeURIComponent(document.getElementById("bus_name").value);
            var bus_number = encodeURIComponent(document.getElementById("bus_number").value);
            var from_place = encodeURIComponent(document.getElementById("from_place").value);
            var to_place = encodeURIComponent(document.getElementById("to_place").value);
            var date = encodeURIComponent(document.getElementById("date").value);
            var arrival_time = encodeURIComponent(document.getElementById("arrival_time").value);
            var departure_time = encodeURIComponent(document.getElementById("departure_time").value);
            var seat_count1 = encodeURIComponent(document.getElementById("seat_count1").value);
            var seat_count2 = encodeURIComponent(document.getElementById("seat_count2").value);
            var seat_type_id1 = encodeURIComponent(document.getElementById("seat_type_id1").value);
            var seat_type_id2 = encodeURIComponent(document.getElementById("seat_type_id2").value);
            var ac_type_id1 = encodeURIComponent(document.getElementById("ac_type_id1").value);
            var rate1 = encodeURIComponent(document.getElementById("rate1").value);
            var rate2 = encodeURIComponent(document.getElementById("rate2").value);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    outputbox(this.responseText);
                }
            }
            xmlHttp.open("post", "AddMultipleTypeBus?bus_name="+bus_name+"&bus_number="+bus_number+"&from_place="+from_place+"&to_place="+to_place+"&date="+date+"&arrival_time="+arrival_time+"&departure_time="+departure_time+"&seat_count1="+seat_count1+"&seat_count2="+seat_count2+"&seat_type_id1="+seat_type_id1+"&seat_type_id2="+seat_type_id2+"&ac_type_id1="+ac_type_id1+"&rate1="+rate1+"&rate2="+rate2); 
            xmlHttp.send();
        }


        function showBusList(){

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    BusListresult(this.responseText);
                }
            }
            xmlHttp.open("post", "BusList"); 
            xmlHttp.send();
        }

        function BusListresult(BusList){
            var a = JSON.parse(BusList);

            for(var i=0;i<a.length;i++){

                var bus_name   = a[i].bus_name;
                var bus_number = a[i].bus_number;

                var BusListdiv = document.getElementById("showBusNameList");

                var MainDivContainer = document.createElement("DIV"); 
                MainDivContainer.setAttribute("id","MainDivContainer");
                MainDivContainer.setAttribute("class","D113_ShowBusNameList");

                var BusNameContainer = document.createElement("DIV"); 
                BusNameContainer.setAttribute("id",bus_name);

                var BusNameContainerText = document.createTextNode(bus_name);
                BusNameContainer.appendChild(BusNameContainerText);

                var BusNumberContainer = document.createElement("DIV"); 
                BusNumberContainer.setAttribute("id",bus_number);

                var BusNumberContainerText = document.createTextNode(bus_number);
                BusNumberContainer.appendChild(BusNumberContainerText);

                MainDivContainer.appendChild(BusNameContainer);
                MainDivContainer.appendChild(BusNumberContainer);
                
                BusListdiv.appendChild(MainDivContainer);

            }


            for(var j=0;j<a.length;j++){

                var bus_name   = a[j].bus_name;
                var bus_number = a[j].bus_number;

                var removeBusList = document.getElementById("removeBusList");

                var MainDivContainer = document.createElement("DIV"); 
                MainDivContainer.setAttribute("id","MainDivContainer");
                MainDivContainer.setAttribute("class","D113_ShowBusNameList");

                var BusNameContainer = document.createElement("DIV"); 
                BusNameContainer.setAttribute("id",bus_name);

                var BusNameContainerText = document.createTextNode(bus_name);
                BusNameContainer.appendChild(BusNameContainerText);

                var BusNumberContainer = document.createElement("DIV"); 
                BusNumberContainer.setAttribute("id",bus_number);

                var BusNumberContainerText = document.createTextNode(bus_number);
                BusNumberContainer.appendChild(BusNumberContainerText);

                MainDivContainer.appendChild(BusNameContainer);
                MainDivContainer.appendChild(BusNumberContainer);

                removeBusList.appendChild(MainDivContainer);

            }
        }

        function showFetchBusByDate(){

            var fetchbusbydate = encodeURIComponent(document.getElementById("fetchbusbydate").value);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    FetchBusByDateresult(this.responseText);
                }
            }
            xmlHttp.open("post", "FetchBusByDate?date="+fetchbusbydate); 
            xmlHttp.send();
        }

        function FetchBusByDateresult(FetchBusByDate){
            var a = JSON.parse(FetchBusByDate);

             const myNode = document.getElementById("showfetchbusbydate");
             while (myNode.firstChild) {
                myNode.removeChild(myNode.lastChild);
             }

            for(var i=0;i<a.length;i++){

                var bus_name   = a[i].bus_name;
                var bus_number = a[i].bus_number;

                var BusListdiv = document.getElementById("showfetchbusbydate");

                var MainDivContainer = document.createElement("button"); 
                MainDivContainer.setAttribute("id","MainDivContainer");
                 MainDivContainer.setAttribute("value",bus_number);
                MainDivContainer.setAttribute("class","D114_TicketBooked");
                MainDivContainer.onclick = function() { 
                    ShowTicketBooked(this);
                };

                var BusNameContainer = document.createElement("DIV"); 
                BusNameContainer.setAttribute("id",bus_name);

                var BusNameContainerText = document.createTextNode(bus_name);
                BusNameContainer.appendChild(BusNameContainerText);

                var BusNumberContainer = document.createElement("DIV"); 
                BusNumberContainer.setAttribute("id",bus_number);

                var BusNumberContainerText = document.createTextNode(bus_number);
                BusNumberContainer.appendChild(BusNumberContainerText);

                MainDivContainer.appendChild(BusNameContainer);
                MainDivContainer.appendChild(BusNumberContainer);
                
                BusListdiv.appendChild(MainDivContainer);

            }
        }

        function ShowTicketBooked(TicketBooked){
            
            var bus_number = TicketBooked.value;
            console.log(bus_number);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    FetchBookedTicketsResult(this.responseText,bus_number);
                }
            }
            xmlHttp.open("post", "FetchBookedTickets?bus_number="+bus_number); 
            xmlHttp.send();
        }


        function removebus(){

            var bus_name = encodeURIComponent(document.getElementById("removebus_name").value);
            var bus_number = encodeURIComponent(document.getElementById("removebus_number").value);


            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    outputboxremove(this.responseText,bus_number);
                }
            }
            xmlHttp.open("post", "RemoveBus?bus_name="+bus_name+"&bus_number="+bus_number); 
            xmlHttp.send();


        }


         function FetchBookedTicketsResult(FetchBusByDate,bus_number){
            var a = JSON.parse(FetchBusByDate);

            seatAvailable(bus_number);

             const myNode = document.getElementById("FetchBookedTickets");
             while (myNode.firstChild) {
                myNode.removeChild(myNode.lastChild);
             }

            for(var i=0;i<a.length;i++){

                var name      = a[i].name;
                var mail      = a[i].mail;
                var phone_no  = a[i].phone_no;
                var gender    = a[i].gender;
                var seat_type = a[i].seat_type;
                var seat_id   = a[i].seat_id;

                var BusListdiv = document.getElementById("FetchBookedTickets");

                var MainDivContainer = document.createElement("DIV"); 
                MainDivContainer.setAttribute("id","MainDivContainer");
                 MainDivContainer.setAttribute("value",bus_number);
                MainDivContainer.setAttribute("class","D113_ShowBusNameList");
                MainDivContainer.onclick = function() { 
                    ShowTicketBooked(this);
                };

                var NameContainer = document.createElement("DIV"); 
                NameContainer.setAttribute("id",name);

                var NameContainerText = document.createTextNode(name);
                NameContainer.appendChild(NameContainerText);

                var MailContainer = document.createElement("DIV"); 
                MailContainer.setAttribute("id",mail);

                var MailContainerText = document.createTextNode(mail);
                MailContainer.appendChild(MailContainerText);

                var PhoneNoContainer = document.createElement("DIV"); 
                PhoneNoContainer.setAttribute("id",phone_no);

                var PhoneNoContainerText = document.createTextNode(phone_no);
                PhoneNoContainer.appendChild(PhoneNoContainerText);

                var GenderContainer  = document.createElement("DIV");
                GenderContainer.setAttribute("id",gender);

                var GenderContainerText  = document.createTextNode(gender);
                GenderContainer.appendChild(GenderContainerText);

                var SeatTypeContainer  = document.createElement("DIV");
                SeatTypeContainer.setAttribute("id",seat_type);

                var SeatTypeContainerText = document.createTextNode(seat_type);
                SeatTypeContainer.appendChild(SeatTypeContainerText);

                var SeatIdContainer  = document.createElement("DIV");
                SeatIdContainer.setAttribute("id",seat_id);

                var SeatIdContainerText = document.createTextNode(seat_id);
                SeatIdContainer.appendChild(SeatIdContainerText);

                MainDivContainer.appendChild(NameContainer);
                MainDivContainer.appendChild(MailContainer);
                MainDivContainer.appendChild(PhoneNoContainer);
                MainDivContainer.appendChild(GenderContainer);
                MainDivContainer.appendChild(SeatTypeContainer);
                MainDivContainer.appendChild(SeatIdContainer);

                BusListdiv.appendChild(MainDivContainer);

            }
        }


        function seatAvailable(bus_number){

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    seatCountSetting(this.responseText);
                }
            }
            xmlHttp.open("post", "CommonTicketAvailable?bus_number="+bus_number); 
            xmlHttp.send();  
        }

        function seatCountSetting(seatCount){
            var a=JSON.parse(seatCount);

            const myNode = document.getElementById("seatCountLeft");
             while (myNode.firstChild) {
                myNode.removeChild(myNode.lastChild);
             }

             const myNode1 = document.getElementById("seatCountRight");
             while (myNode1.firstChild) {
                myNode1.removeChild(myNode1.lastChild);
             }

            if (a.length % 2 == 0) {

                for(var i=0;i<a.length;i++){

                    seat_id = a[i].seat_id;
                    status  = a[i].status;
                    var stylecolor =null;
                    var classstyle =null;

                    var seat_id1 = seat_id;
                    seat_id1 =  seat_id1[seat_id1.length-1];

                    if (seat_id1==null) {
                        classstyle ="grid-item"
                    }else if (seat_id1=="O") {
                        console.log("O");
                        classstyle ="grid-item"
                    }else if (seat_id1=="T") {
                        console.log("T");
                        classstyle ="grid-item1"
                    }

                    if (status==0) {
                        stylecolor ="background-color: grey";
                    }else if (status==1) {
                        stylecolor ="background-color: #f94c4c";
                    }

                    if (i<a.length/2+1) {

                        var seatCountLeftdiv=document.getElementById("seatCountLeft");
                        var seatCountLeft = document.createElement("Div");
                        seatCountLeft.setAttribute("id", seat_id);
                        seatCountLeft.setAttribute("value", seat_id);
                        seatCountLeft.setAttribute("style", stylecolor);
                        seatCountLeft.classList.add(classstyle);

                        var seatCountLeftText = document.createTextNode(i+1);
                        seatCountLeft.appendChild(seatCountLeftText);
                        

                        seatCountLeftdiv.appendChild(seatCountLeft);
                    }

                    if (i>a.length/2) {

                        var seatCountRightdiv=document.getElementById("seatCountRight");

                        var seatCountRight = document.createElement("DIV");
                        seatCountRight.setAttribute("id",seat_id);
                        seatCountRight.setAttribute("value",seat_id);
                        seatCountRight.setAttribute("style", stylecolor);
                        seatCountRight.classList.add(classstyle);

                        var seatCountRightText = document.createTextNode(i+1);
                        seatCountRight.appendChild(seatCountRightText);

                        seatCountRightdiv.appendChild(seatCountRight);
                    }
                }
            }else{

                for(var i=0;i<a.length;i++){

                    seat_id = a[i].seat_id;
                    status  = a[i].status;
                    var stylecolor =null;

                    var classstyle =null;

                    var seat_id1 = seat_id;
                    seat_id1 =  seat_id1[seat_id1.length-1];

                    if (seat_id1==null) {
                        classstyle ="grid-item"
                    }else if (seat_id1=="O") {
                        console.log("O");
                        classstyle ="grid-item"
                    }else if (seat_id1=="T") {
                        console.log("T");
                        classstyle ="grid-item1"
                    }


                    if (status==0) {
                        stylecolor ="background-color: grey";
                    }else if (status==1) {
                        stylecolor ="background-color: #f94c4c";
                    }

                    if (i<=a.length/2) {

                        var seatCountLeftdiv=document.getElementById("seatCountLeft");
                        var seatCountLeft = document.createElement("DIV");
                        seatCountLeft.setAttribute("id", seat_id);
                        seatCountLeft.setAttribute("value", seat_id);
                        seatCountLeft.setAttribute("style", stylecolor);
                        seatCountLeft.classList.add(classstyle);

                        var seatCountLeftText = document.createTextNode(i+1);
                        seatCountLeft.appendChild(seatCountLeftText);

                        seatCountLeftdiv.appendChild(seatCountLeft);
                    }

                    if (i>=a.length/2) {

                        var seatCountRightdiv=document.getElementById("seatCountRight");

                        var seatCountRight = document.createElement("DIV");
                        seatCountRight.setAttribute("id",seat_id);
                        seatCountRight.setAttribute("value",seat_id);
                        seatCountRight.setAttribute("style", stylecolor);
                        seatCountRight.classList.add(classstyle);

                        var seatCountRightText = document.createTextNode(i+1);
                        seatCountRight.appendChild(seatCountRightText);

                        seatCountRightdiv.appendChild(seatCountRight);
                    }
                }
                

            }
            
            
        }


        function outputbox(outputvalue){

            var a=JSON.parse(outputvalue);

            var status = a.status;

            var modal = document.getElementById("myModal");
            var outputMsg = document.getElementById("outputMsg");

            modal.style.display = "block";

            if (status=="SUCCESS") {
                outputMsg.innerText ="Bus Successfully Added"; 
            }else{
                outputMsg.innerText ="Bus Not Added";
            }

            okbtn.onclick = function(){
                location.reload();
            }
        }

        function outputboxremove(outputvalue){

            var a=JSON.parse(outputvalue);

            var status = a.status;

            var modal = document.getElementById("myModal");
            var outputMsg = document.getElementById("outputMsg");

            modal.style.display = "block";

            if (status=="SUCCESS") {
                outputMsg.innerText ="Bus Removed Successfully"; 
            }else{
                outputMsg.innerText ="Bus Not Removed";
            }

            okbtn.onclick = function(){
                location.reload();
            }
        }
    </script>
    
   

</body>
</html>