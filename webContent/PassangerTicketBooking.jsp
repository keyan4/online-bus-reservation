<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	.D310_ticketBookingDetails_Container{
		width: 1665px;
		height: 950px;
		background-color: white;
	}
	.D311_inputContainer{
		margin-top: 20px;
		margin-left: 250px;
		width: 200px;
		height: 15px;
		padding: 5px;
		border-radius: 10px;
		display: block;
	}
	.D311_passangerDetailsSubmitButton {
		margin-top: 20px;
		margin-left: 280px;
		padding: 5px;
		width: 120px;
		border-bottom: 1px solid #d84f57;
		border-top: 1px solid #d84f57;
		border-left: 1px solid #d84f57;
		border-right: 1px solid #d84f57;
		border-radius: 20px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 12px;
	}
	.D311_passangerDetailsSubmitButton1 {
		background-color: white; 
		color: black; 
		border: 2px solid #d84f57;
	}
	.D311_passangerDetailsSubmitButton1:hover {
		background-color: #d84f57;
		border: 2px solid #fff;
		color: white;
	}
	.D320_paymentDetails_Container{
		width: 1665px;
		height: 950px;
		background-color: white;
	}
	
	.grid-container {
		display: grid;
		grid-column-gap: 10px;
		grid-row-gap: 10px;
		grid-template-columns: auto auto;
		background-color: white;
		padding: 30px;
		}
	.grid-item {
		background-color: white;
		border: 1px solid rgba(0, 0, 0, 0.8);
		padding: 5px;
		font-size: 20px;
		text-align: center;
	}
	.grid-item1 {
		background-color: white;
		
		height: 100px;
		border: 1px solid rgba(0, 0, 0, 0.8);
		padding: 5px;
		font-size: 20px;
		text-align: center;
	}
	.bothgrid{
		margin-top: 50px;
		float: left;
		width: 400px;
		height: 850px;
		background-color: white;
		display: inline-block;
		overflow: scroll;
		box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);

	}
	.gridsize{
		float: left;
		margin-top: 75px;
		width: 200px;
		height: 900px;
		background-color: white;
		display: inline-block;
		
	}
	.gridsize1{
		margin-top: 50px;
		margin-left: 100px;
		float: left;
		width: 900px;
		height: 850px;
		background-color: white;
		display: inline-block;
		overflow: scroll;
		box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
	}
	.gridsize1sub{
		margin-top: 50px;
		margin-left: 100px;
		float: left;
		width: 700px;
		height: 500px;
		background-color: white;
		display: inline-block;
		overflow: scroll;
		
	}
	.redcolordiv{
		background-color: red;
		
	}

	.modal {
		display: none;
		position: fixed;
		z-index: 1;
		padding-top: 100px;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		overflow: auto;
		background-color: rgb(0,0,0);
		background-color: rgba(0,0,0,0.4);
	}

	.modal-content {
		background-color: #fff;
		margin: auto;
		padding: 20px;
		border: 1px solid #888;
		margin-top: 200px;
		width: 400px;
	}
	.modal-text{
		margin-left: 70px;
		font-size: 20px;
		width: 400px;
	}
	.model-button{
		margin-left: 150px;
		width: 100px;
		padding: 5px;
	}

	.close {
		color: dodgerblue;
		float: right;
		font-size: 28px;
		font-weight: bold;
	}
	.close:hover,
	.close:focus {
		color: #000;
		text-decoration: none;
		cursor: pointer;
	}

</style>
</head>
<body onload="allFunction()">
	<div class="D310_ticketBookingDetails_Container">
		<div class="bothgrid" style="margin-left: 100px;" id="SeatDiv">
			<div class="gridsize" >
				<div class="grid-container" id="seatCountLeft"></div>
			</div>
			<div class="gridsize">
				<div class="grid-container" id="seatCountRight"></div>
			</div>	
		</div>
		<div class="gridsize1" id="customerDetailsMainDiv">
			<div class="gridsize1sub" id="customerDetails">
				<input class="D311_inputContainer" style="margin-top: 70px;" id="username" type="text" name="username" placeholder="User Name">
				<input class="D311_inputContainer" id="gender" type="text" name="gender" placeholder="Gender">
				<input class="D311_inputContainer" id="mail" type="text" name="mail" placeholder="E-Mail">
				<input class="D311_inputContainer" id="phone_no" type="text" name="phone_no" placeholder="Phone No">
				<input class="D311_inputContainer" id="cardnumber" type="text" name="cardnumber" placeholder="Card Number">
				<input class="D311_inputContainer" id="ccvnumber" type="text" name="ccvnumber" placeholder="CCV Number">
				<input class="D311_passangerDetailsSubmitButton D311_passangerDetailsSubmitButton1" type="submit" value="Submit" onclick="gettingPersonDetails()">
			</div>
			
		</div>
	</div>

	<div id="myModal" class="modal">
		<div class="modal-content">
			<p id="outputMsg" class="modal-text"></p>
			<button id="okbtn" class="model-button">ok</button>
		</div>
	</div>
	

	<script type="text/javascript">
		var CommonBus_Number = null;
		var array = [];
		var passangerDetails=[];
		var commonMail = null;
		var CommonPhoneNo =null;
		var CommonCardNumber =null;
		var CommonCCV = null;


        function seatAvailable(bus_number){

        	var xmlHttp = new XMLHttpRequest();
    		xmlHttp.onreadystatechange = function()
    		{
    			if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
    			{
    				seatCountSetting(this.responseText);
    			}
    		}
    		xmlHttp.open("post", "CommonTicketAvailable?bus_number="+bus_number); 
    		xmlHttp.send();  
        }

        function seatCountSetting(seatCount){
        	var a=JSON.parse(seatCount);

        	if (a.length % 2 == 0) {

        		for(var i=0;i<a.length;i++){

        			seat_id = a[i].seat_id;
        			status  = a[i].status;
        			var stylecolor =null;
        			var classstyle =null;

        			var seat_id1 = seat_id;
        			seat_id1 =  seat_id1[seat_id1.length-1];

        			if (seat_id1==null) {
        				classstyle ="grid-item"
        			}else if (seat_id1=="O") {
        				console.log("O");
        				classstyle ="grid-item"
        			}else if (seat_id1=="T") {
        				console.log("T");
        				classstyle ="grid-item1"
        			}

        			if (status==0) {
        				stylecolor ="background-color: grey";
        			}else if (status==1) {
        				stylecolor ="background-color: #f94c4c";
        			}

        			if (i<a.length/2+1) {

        				var seatCountLeftdiv=document.getElementById("seatCountLeft");
        				var seatCountLeft = document.createElement("button");
        				seatCountLeft.setAttribute("id", seat_id);
        				seatCountLeft.setAttribute("value", seat_id);
        				seatCountLeft.setAttribute("style", stylecolor);
        				seatCountLeft.classList.add(classstyle);

        				var seatCountLeftText = document.createTextNode(i+1);
        				seatCountLeft.appendChild(seatCountLeftText);
        				seatCountLeft.onclick = function() { 
        					seatSelectingProcess(this)
        				};

        				seatCountLeftdiv.appendChild(seatCountLeft);
        		    }

        		    if (i>a.length/2) {

        		    	var seatCountRightdiv=document.getElementById("seatCountRight");

        		    	var seatCountRight = document.createElement("button");
        		    	seatCountRight.setAttribute("id",seat_id);
        		    	seatCountRight.setAttribute("value",seat_id);
        		    	seatCountRight.setAttribute("style", stylecolor);
        		    	seatCountRight.classList.add(classstyle);

        		    	var seatCountRightText = document.createTextNode(i+1);
        		    	seatCountRight.appendChild(seatCountRightText);
        		    	seatCountRight.onclick = function() { 
        		    		seatSelectingProcess(this)
        		    	};

        		    	seatCountRightdiv.appendChild(seatCountRight);
        		    }
        		}
        	}else{

        		for(var i=0;i<a.length;i++){

        			seat_id = a[i].seat_id;
        			status  = a[i].status;
        			var stylecolor =null;

        			var classstyle =null;

        			var seat_id1 = seat_id;
        			seat_id1 =  seat_id1[seat_id1.length-1];

        			if (seat_id1==null) {
        				classstyle ="grid-item"
        			}else if (seat_id1=="O") {
        				console.log("O");
        				classstyle ="grid-item"
        			}else if (seat_id1=="T") {
        				console.log("T");
        				classstyle ="grid-item1"
        			}


        			if (status==0) {
        				stylecolor ="background-color: grey";
        			}else if (status==1) {
        				stylecolor ="background-color: #f94c4c";
        			}

        			if (i<=a.length/2) {

        				var seatCountLeftdiv=document.getElementById("seatCountLeft");
        				var seatCountLeft = document.createElement("button");
        				seatCountLeft.setAttribute("id", seat_id);
        				seatCountLeft.setAttribute("value", seat_id);
        				seatCountLeft.setAttribute("style", stylecolor);
        				seatCountLeft.classList.add(classstyle);

        				var seatCountLeftText = document.createTextNode(i+1);
        				seatCountLeft.appendChild(seatCountLeftText);
        				seatCountLeft.onclick = function() { 
        					seatSelectingProcess(this)
        				};

        				seatCountLeftdiv.appendChild(seatCountLeft);
        		    }

        		    if (i>=a.length/2) {

        		    	var seatCountRightdiv=document.getElementById("seatCountRight");

        		    	var seatCountRight = document.createElement("button");
        		    	seatCountRight.setAttribute("id",seat_id);
        		    	seatCountRight.setAttribute("value",seat_id);
        		    	seatCountRight.setAttribute("style", stylecolor);
        		    	seatCountRight.classList.add(classstyle);

        		    	var seatCountRightText = document.createTextNode(i+1);
        		    	seatCountRight.appendChild(seatCountRightText);
        		    	seatCountRight.onclick = function() { 
        		    		seatSelectingProcess(this)
        		    	};

        		    	seatCountRightdiv.appendChild(seatCountRight);
        		    }
        		}
        		

        	}
        	
        	
        }

        function seatSelectingProcess(seat_id){

        	

        	var divid = document.getElementById(seat_id.id);
        	var divColor = seat_id.style.backgroundColor;

        	if (divColor=="rgb(37, 243, 37)") {
        		document.getElementById(seat_id.id).style.backgroundColor = "grey";
        		const index = array.indexOf(seat_id.id);
        		if (index > -1) {
        			array.splice(index, 1);
        		}

        	}else if (divColor=="grey") {
        		document.getElementById(seat_id.id).style.backgroundColor = "#25f325";
        		array.push(seat_id.id);
        	}

        	
        }
		
		function gettingPersonDetails(){

			var username = encodeURIComponent(document.getElementById("username").value);
			var gender   = encodeURIComponent(document.getElementById("gender").value);
			var mail     = encodeURIComponent(document.getElementById("mail").value);
			var phone_no = encodeURIComponent(document.getElementById("phone_no").value);
			var cardnumber = encodeURIComponent(document.getElementById("cardnumber").value);
			var ccvnumber  = encodeURIComponent(document.getElementById("ccvnumber").value);

			CommonCardNumber = cardnumber;
			CommCCV          = ccvnumber;
			commonMail = mail;
			CommonPhoneNo = phone_no;

			if (array.length==0) {

				alert("Please select any Seat");

			}else if (array.length==1) {

				
				passangerDetails.push({
					"username"  : username,
					"gender"    : gender,
					"mail"      : mail,
					"phone_no"  : phone_no,
					"cardnumber": cardnumber,
					"ccvnumber" : ccvnumber,
					"seat_id"   : array[0]
				})

				ticketBookingProcess()
				

			}else{

				passangerDetails.push({
					"username"  : username,
					"gender"    : gender,
					"mail"      : mail,
					"phone_no"  : phone_no,
					"cardnumber": cardnumber,
					"ccvnumber" : ccvnumber,
					"seat_id"   : array[0]
				})

				var nodes = document.getElementById("customerDetails").getElementsByTagName('*');
				for(var i = 0; i < nodes.length; i++){
					nodes[i].style.display = "none";
				}

				createCustomerDetailDiv()
				
			}

		}

		function createCustomerDetailDiv(){

			for(var i=1;i<array.length;i++){

				var customerDetails1=document.getElementById("customerDetails");

				var customerDetails1UserName = document.createElement("input"); 
				customerDetails1UserName.setAttribute("id","username"+i);
				customerDetails1UserName.setAttribute("name","username"+i);
				customerDetails1UserName.setAttribute("placeholder","User Name");
				customerDetails1UserName.setAttribute("class","D311_inputContainer");

				var customerDetails1Gender = document.createElement("input"); 
				customerDetails1Gender.setAttribute("id","gender"+i);
				customerDetails1Gender.setAttribute("name","gender"+i);
				customerDetails1Gender.setAttribute("placeholder","Gender");
				customerDetails1Gender.setAttribute("class","D311_inputContainer");
				customerDetails1.appendChild(customerDetails1UserName);
				customerDetails1.appendChild(customerDetails1Gender);

			}
			var customerDetails1SubmitButton = document.createElement("button"); 
				customerDetails1SubmitButton.setAttribute("id","submitButton");
				customerDetails1SubmitButton.setAttribute("value","Submit");
				customerDetails1SubmitButton.setAttribute("class","D311_passangerDetailsSubmitButton D311_passangerDetailsSubmitButton1");
				customerDetails1SubmitButton.onclick = function() { 
                    
                    ticketBookingProcess1()
                };

			customerDetails1.appendChild(customerDetails1SubmitButton);
            

		}

		function ticketBookingProcess1(){

			for(var i=1;i<array.length;i++){

				var username = encodeURIComponent(document.getElementById("username"+i).value);
				var gender = encodeURIComponent(document.getElementById("gender"+i).value);

				passangerDetails.push({
					"username"  : username,
					"gender"    : gender,
					"mail"      : mail.value,
					"phone_no"  : phone_no.value,
					"cardnumber": cardnumber.value,
					"ccvnumber" : ccvnumber.value,
					"seat_id"   : array[i]
				})
			}

			ticketBookingProcess1sub()

		}

		function ticketBookingProcess1sub(){
			var j = 0;

			for(var i=0;i<passangerDetails.length;i++){
				var username   = passangerDetails[i].username;
				var gender     = passangerDetails[i].gender;
				var mail       = passangerDetails[i].mail;
				var phone_no   = passangerDetails[i].phone_no;
				var cardnumber = passangerDetails[i].cardnumber;
				var ccvnumber  = passangerDetails[i].ccvnumber;
				var seat_id    = passangerDetails[i].seat_id;

				j++;

				var xmlHttp = new XMLHttpRequest();
				xmlHttp.onreadystatechange = function()
				{
					if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
					{
						outputbox(this.responseText,j);
					}
				}
				xmlHttp.open("post", "CommonTicketBooked?bus_number="+CommonBus_Number+"&name="+username+"&mail="+mail+"&phone_no="+phone_no+"&gender="+gender+"&card_number="+cardnumber+"&ccv_number="+ccvnumber+"&seat_id="+seat_id); 
				xmlHttp.send(); 
			}

			

		}

		function ticketBookingProcess(){

			for(var i=0;i<passangerDetails.length;i++){
				var username   = passangerDetails[i].username;
				var gender     = passangerDetails[i].gender;
				var mail       = passangerDetails[i].mail;
				var phone_no   = passangerDetails[i].phone_no;
				var cardnumber = passangerDetails[i].cardnumber;
				var ccvnumber  = passangerDetails[i].ccvnumber;
				var seat_id    = passangerDetails[i].seat_id;

				var xmlHttp = new XMLHttpRequest();
				xmlHttp.onreadystatechange = function()
				{
					if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
					{
						outputbox(this.responseText);
					}
				}
				xmlHttp.open("post", "CommonTicketBooked?bus_number="+CommonBus_Number+"&name="+username+"&mail="+mail+"&phone_no="+phone_no+"&gender="+gender+"&card_number="+cardnumber+"&ccv_number="+ccvnumber+"&seat_id="+seat_id); 
				xmlHttp.send(); 
			}


		}


		function outputbox(outputvalue){

			var a=JSON.parse(outputvalue);

			var status = a.status;

			var modal = document.getElementById("myModal");
			var outputMsg = document.getElementById("outputMsg");

			modal.style.display = "block";

			if (status=="SUCCESS") {
				outputMsg.innerText ="Ticket Booked SuccessFully"; 
			}else{
				outputMsg.innerText ="Ticket Not Booked";
			}

			okbtn.onclick = function(){
				window.location.href = "http://localhost:8010/BusReservation/Passanger.jsp";
			}
		}

		



		function firstfunction(){
            var url_string =window.location.href;
            var url = new URL(url_string);
            var BusNumber = url.searchParams.get("BusNumber");

            return BusNumber;
        }

		
		function allFunction() {
			
			CommonBus_Number = firstfunction()
			seatAvailable(CommonBus_Number)
		}


		


	</script>
</body>
</html>