<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bus Reservation</title>
<style type="text/css">
	.D110_main_upper_container{
		background-color: #d84f57;
		display: inline-block;
		width: 1685px;
		height: 50px;
		margin-top: -10px;
		margin-left: -10px;
	}
	.D110_upper_divcontainer{
		display: inline-block;
		margin-left: 1500px;
		margin-top: 10px;
	}
	.D111_main_mid_container{
		background-image: url("./Img/redbusimg.png");
		width: 1685px;
		height: 580px;
		margin-left: -10px;
		display: block;
	}
	.D111_input_divcontainer{
		display: inline-block;
		margin-left: 300px;
		margin-top: 280px;
	}
	.D111_input_valuecontainer{
		padding: 15px;
		display: inline-block;
		margin-top: 20px;
		width: 260px;
	}
	.D110_signin_up_button {
		padding: 10px;
		width: 120px;
		border-bottom: 1px solid #d84f57;
		border-top: 1px solid #d84f57;
		border-left: 1px solid #d84f57;
		border-right: 1px solid #d84f57;
		border-radius: 20px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 12px;
	}
	.D110_signin_up_button1 {
		background-color: white; 
		color: black; 
		border: 2px solid #d84f57;
	}
	.D110_signin_up_button1:hover {
		background-color: #d84f57;
		border: 2px solid #fff;
		color: white;
	}

	.D113_adervisement-tile {
		display: flex;
		flex-direction: row;
		background: #fff;
		width: 750px;
		padding: 5px;
		margin-left: 420px;
		min-height: 105px;
		margin-top: 11em;
		box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
		justify-content: center;
		align-items: center;
    }
    .D113_img-section {
    	margin-left: 40px;
    	width: 250px;
    }
    .D113_mt_heading {
    	font-size: 22px;
    	text-align: left;
    }
    .D113_sub-text {
    	font-size: 13px;
    	text-align: left;
    	color: #9b9b9b;
    }
    .D116_offer_box {
    	display: flex;
    	width: 23%;
    	background-color: #fff;
    	margin-top: 120px;
    	margin-left: auto;
    	padding: 10px;
    	box-shadow: 0 1px 10px #c3c3c3;
    	vertical-align: top;
    	margin-right: auto;
    	justify-content: center;
    	align-items: center;
    }
    .D116_offer_title {
    	margin-bottom: 10px;
    	font-family: Lato,sans-serif;
    }
    .D116_container {
    	max-width: 1013px;
    	font-family: Lato,sans-serif;
    } 
    .D116_offer-footer {
    	margin-top: 10px;
    	font-family: Lato,sans-serif;
    }
    .D116_offer a {
    	color: #000;
    	text-decoration: none;
    }

    .D151_outerContainer {
    	margin: 1em 0;
    	text-align: center;
    	font-family: Lato,sans-serif;
    }
    .D151_heading {
    	font-size: 24px;
    	text-align: center;
    	color: #404040;
    	font-family: Lato,sans-serif;
    }
    .D151_container {
    	display: table;
    	width: 84%;
    	margin: 1em auto auto;
    	border: 1px solid #cecece;
    }
    .D151_promiseContainer {
    	display: table-cell;
    	padding: 26px;
    	vertical-align: top;
    	border-left: 1px solid #cecece;
    	min-width: 250px;
    	font-family: Lato,sans-serif;
    }
    .D151_imageContainer {
    	width: 300px;
    	height: 7em;
    	margin: auto;
    }
    .D151_promiseTitle {
    	text-align: center;
    	font-family: Lato,sans-serif;
    	font-size: 16px;
    	margin-top: 35px;
    }
    .D151_promiseText {
    	text-align: center;
    	font-family: Lato,sans-serif;
    	font-size: 16px;
    	padding: 26px;
    	color: #686868;
    	width: 12vw;
    	margin: 5px auto auto;

    }

    .D210_filter_container{
    	display: inline-block;
    	width: 300px;
    	height: 950px;
    	background-color: #fff;
    }
    .D211_bus_view_container{
    	display: inline-block;
    	margin-left: -5px;
    	width: 1355px;
    	height: 950px;
    	background-color: #fff;
    }
    .D211_bus_deal_container{
    	margin-top: 10px;
    	display: block;
    	width: 1360px;
    	height: 280px;
    	background-color: #fff;
    	
    }
    .D211_bus_deal_1{
    	display: inline-block;
    	margin-top: 10px;
    	width: 250px;
    	height: 220px;
    	margin-left: 60px;
    	background-color: aqua;
    	box-shadow: 0 3px 7px 0 rgba(0,0,0,.28);
    }
    .D211_bus_list_container{
    	display: block;
    	width: 1360px;
    	height: 700px;
    	background-color: #fff;
    	overflow: scroll;
    }
    .D212_bus_list_button{
    	width :1360px;
    	margin-top: 30px;
    	border-top: 1px solid grey;
    	border-bottom: 1px solid grey;
    	border-right: 1px solid grey;
    	border-left: 1px solid grey;
    	height: 140px;
    	padding: 10px;
    }
    .D212_Button_Div_Container_Left{
    	    float: left;
    	    width: 675px;
    	    height: 120px;
    }
    .D212_Button_Div_Container_Right{
        width: 660px;
        height: 120px;
        float: right;
    }
    .D213_Bus_Name_Container{
    	display: inline-block;
    	width: 650px;
    	margin-top: 20px;
    	padding: 10px;
    	text-align: left;
    	font-size: 17px;
    	float: left;
    	text-transform: capitalize;
    	font-family: serif;
    }
    .D213_From_And_To_Place_Container{
    	display: inline-block;
    	width: 660px;
    	font-size: 15px;
    	padding: 5px;
    	text-align: left;
    	float: left;
    	text-transform: capitalize;
    	font-family: serif;
    }
    .D213_Seat_Count_Container{
    	display: inline-block;
    	width: 330px;
    	float: left;
    	height: 10px;
    	padding: 15px;
    	text-align: left;
    	font-family: serif;
    }
    .D213_Ac_Type_Container{
    	display: inline-block;
    	width: 280px;
    	height: 10px;
    	float: left;
    	padding: 15px;
    	text-align: left;
    	text-transform: capitalize;
    	font-family: serif;
    }
    .D213_Date_Time_Container{
    	display: block;
    	width: 630px;
    	height: 10px;
    	padding: 10px;
    	font-size: 15px;
    	text-align: right;
    	margin-top: 20px;
    }
    .D213_Rate_Container{
    	display: block;
    	width: 620px;
    	height: 10px;
    	padding: 10px;
    	font-size: 15px;
    	text-transform: capitalize;
    	text-align: right;
    	margin-top: 40px;
    }

    .modal {
        display: none; 
        position: fixed; 
        z-index: 1; 
        padding-top: 100px; 
        left: 0;
        top: 0;
        width: 100%; 
        height: 100%;
        overflow: auto; 
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0,0.6);
    }
    .modal-content {
        background-color: #fff;
        margin: auto;
        width: 850px;
        height: 400px;
        margin-top: 150px;
        margin-left: 450px;
        border-bottom: 1px solid #3c3c3d87;
        border-top: 1px solid #3c3c3d87;
        border-left: 1px solid #3c3c3d87;
        border-right: 1px solid #3c3c3d87;
        border-radius: 10px;
        padding: 10px;
    }
    .close {
        color: dodgerblue;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }


    
    .D112_sign_back_button {
        background-color: #d84f57;
        width: 30px;
        height: 30px;
        border-radius: 50px;
        text-align: center;
        display: block;
        margin-left: 370px;
    }
    .D112_input_divcontainerLeft{
        display: inline-block;
        width: 400px;
        height: 400px;
        border-right: 1px solid black;
    }
    .D112_input_divcontainerRight{
        display: inline-block;
        width: 400px;
        height: 400px;
        float: right;
    }
    .D112_input_valuecontainer{
        width: 200px;
        padding: 5px;
        margin-top: 20px;
        margin-left: 100px;
        border-radius: 10px;
        display: block;

    }

    .D112_login_button{
        margin-top: 30px;
        width: 200px;
        padding: 5px;
        margin-left: 100px;
        border-radius: 10px;
        display: block;
    }

    .D114_signup_button{
        margin-top: 30px;
        width: 200px;
        margin-left: 100px;
        padding: 5px;
        border-radius: 10px;
        display: block;
    }
    
</style>
</head>


<body onload="allfunctions()">

	<div id="SearchBus" style="display: none;">
	<div class="D110_main_upper_container" id="D110_main_upper_container">
		<div class="D110_upper_divcontainer">
			<input id="singninupbutton" class="D110_signin_up_button D110_signin_up_button1" type="button" value="SignIn / SignUp" />
		</div>
	</div>
	<div class="D111_main_mid_container" id="D111_main_mid_container">
		<div class="D111_input_divcontainer" id="D111_input_divcontainer">
			
				<input class="D111_input_valuecontainer" list="from_place_datalist" name="from_place" placeholder="From Place" id="from_place_id" autocomplete="of">
				<datalist id="from_place_datalist">
				</datalist>
				<input class="D111_input_valuecontainer" list="to_place" name="to_place" placeholder="To Place" id="to_place_id" autocomplete="of">
				<datalist id="to_place">
				</datalist>
				<input class="D111_input_valuecontainer" type="date" name="date_time" id="date_time" placeholder="Date">
				
				<button class="D111_input_valuecontainer" id="search_bus" style="background-color: #d84f57" onclick="BusSearchCommon()">Search Bus </button>
		</div>
		<div class="D113_adervisement-tile" id="D113_adervisement-tile">
		<div class="D113_img-section">
			<img src="https://s1.rdbuz.com/images/MobileOffers/amazon/AMAZON80.png">
		</div>
		<div class="D120_details-section">
			<b>
				<div class="D113_mt_heading">Win Rs 10 to Rs 300 on minimum purchase of Rs 300.
				</div>
			</b>
			<div class="D113_sub-text">AMAZON pay offer
			</div>
		</div>
	</div>
	</div>

	<div class="D116_offer_box" id="D116_offer_box">
		<a class="D116_offer_anchor">
		<div class="D116_offer_title">Save up to Rs 150 on bus tickets</div>
		<div class="D116_container">
			<img src="https://st.redbus.in/Images/INDOFFER/FESTIVE/Newjan/274x147.png">
		</div>
		<div class="D116_offer-footer">
			<b>Limited Period Offer</b>
		</div>
		<br>
		<div class="D116_offer_desc">App Only Offer. Use FIRST
		</div>
	    </a>
	</div>

	<div class="D151_outerContainer" id="D151_outerContainer">
		<h2 class="D151_heading">WHY BOOK WITH WHITE BUS</h2>
		<div class="D151_container">
			<div class="D151_promiseContainer">
				<div class="D151_imageContainer">
					<img class="D151_image" src="//s3.rdbuz.com/web/images/home/maximum_choices.png">
				</div>
				<div class="D151_promiseTitle">MAXIMUM CHOICE
				</div>
				<div class="D151_promiseText">We give you the widest number of travel options across thousands of routes.
				</div>
			</div>
			<div class="D151_promiseContainer">
				<div class="D151_imageContainer">
					<img class="D151_image" src="//s1.rdbuz.com/web/images/home/customer_care.png">
				</div>
				<div class="D151_promiseTitle">SUPERIOR CUSTOMER SERVICE
				</div>
				<div class="D151_promiseText">We put our experience and relationships to good use and are available to solve your travel issues.
				</div>
			</div>
			<div class="D151_promiseContainer">
				<div class="D151_imageContainer">
					<img class="D151_image" src="//s1.rdbuz.com/web/images/home/lowest_Fare.png">
				</div>
				<div class="D151_promiseTitle">LOWEST PRICES
				</div>
				<div class="D151_promiseText">We always give you the lowest price with the best partner offers.
				</div>
			</div>
			<div class="D151_promiseContainer">
				<div class="D151_imageContainer">
					<img class="D151_image" src="//s2.rdbuz.com/web/images/home/benefits.png">
				</div>
				<div class="D151_promiseTitle">UNMATCHED BENEFITS
				</div>
				<div class="D151_promiseText">We take care of your travel beyond ticketing by providing you with innovative and unique benefits.
				</div>
			</div>
		</div>
	</div>
	</div>
	<div id="BusList" style="display: none;">
		<div class="D210_filter_container">
			
		</div>
		<div class="D211_bus_view_container">
			<div class="D211_bus_deal_container" id="D211_bus_deal_container">
				<div class="D211_bus_deal_1">
					<img src="./Img/busdeal_1.jpeg" width="250px" height="220px">
				</div>
				<div class="D211_bus_deal_1">
					<img src="./Img/busdeal_2.jpeg" width="250px" height="220px">
				</div>
				<div class="D211_bus_deal_1">
					<img src="./Img/busdeal_3.jpeg"  width="250px" height="220px">
				</div>
				<div class="D211_bus_deal_1">
					<img src="./Img/busdeal_4.jpeg"  width="250px" height="220px">
				</div>
			</div>
			<div class="D211_bus_list_container" id="D211_bus_list_container">
				<div id="bus_list_container">
					
				</div>
				
			</div>
			
		</div>
	</div>

    <div id="myModal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            
            <div class="D112_input_divcontainerLeft">
            <form action="LogIn" method="post">
            <input class="D112_input_valuecontainer" type="text" name="userName" id="userName" placeholder="User Name" style="margin-top: 60px;">
            <input class="D112_input_valuecontainer" type="password" name="password" id="password" placeholder="Password">
            <input class="D112_login_button" type="submit" name="submit" value="Log In">
            </form>
            </div>
            <div class="D112_input_divcontainerRight">
            <input class="D112_input_valuecontainer" type="text" name="username" id = "username" placeholder="User Name" style="margin-top: 60px;">
            <input class="D112_input_valuecontainer" type="text" name="mail" id ="mail" placeholder="Mail">
            <input class="D112_input_valuecontainer" type="text" name="phone_no" id ="phone_no" placeholder="Phone No ">
            <input class="D114_signup_button" type="submit" value="Create New Account" onclick="InsertDatacreatepassanger()">
        </div>
            
        </div>
    </div>

    <script type="text/javascript">

    	function ShowFromPlace(){
    		var xmlHttp = new XMLHttpRequest();
    		xmlHttp.onreadystatechange = function()
    		{
    			if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
    			{
    				ShowFromPlaceValue(this.responseText);
    			}
    		}
    		xmlHttp.open("post", "ShowFromPlace"); 
    		xmlHttp.send(); 
    	}

    	function ShowFromPlaceValue(from_place){
    		var a=JSON.parse(from_place);
    		var data = [];
    		var list = document.getElementById('from_place_datalist');

    		for (var i = 0; i < a.length; i++) {
    			var from_place = a[i].from_place;
    			data.push(from_place);
				
			}

			data.forEach(function(item){
				var option = document.createElement('option');
				option.value = item;
				list.appendChild(option);
			});
			
    	}

    	function ShowToPlace(){
    		var xmlHttp = new XMLHttpRequest();
    		xmlHttp.onreadystatechange = function()
    		{
    			if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
    			{
    				ShowToPlaceValue(this.responseText);
    			}
    		}
    		xmlHttp.open("post", "ShowToPlace"); 
    		xmlHttp.send(); 
    	}

    	function ShowToPlaceValue(to_place){
    		var a=JSON.parse(to_place);
    		var data = [];
    		var list = document.getElementById('to_place');

    		for (var i = 0; i < a.length; i++) {
    			var to_place = a[i].to_place;
    			data.push(to_place);
				
			}

			data.forEach(function(item){
				var option = document.createElement('option');
				option.value = item;

				list.appendChild(option);
			});
			
    	}

    	function valueshow() {
    		var getvalue  =document.getElementsByTagName('from_place')[0];
    		getvalue.addEventListener('input',function()
    			{
    				document.getElementById('spanid').innerHTML ="output "+this.value;
    			});
    	}
    	

    	function BusSearchCommon(){

    		var div_id="BusList";
    		var from_place =encodeURIComponent(document.getElementById("from_place_id").value);

    		var to_place =encodeURIComponent(document.getElementById("to_place_id").value);
    		var date_time=encodeURIComponent(document.getElementById("date_time").value);


    		document.getElementById("SearchBus").style.display="none";
        	document.getElementById("BusList").style.display="block";

			urlappendingprocess(div_id,from_place,to_place,date_time);


    		var xmlHttp = new XMLHttpRequest();
    		xmlHttp.onreadystatechange = function()
    		{
    			if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
    			{
    				resultofsearchbus(this.responseText)
    			}
            }
            xmlHttp.open("post", "CommonBusSearch?from_place="+from_place+"&to_place="+to_place+"&date_time="+date_time);
            xmlHttp.send();
        }

        function resultofsearchbus(result){
        	var a=JSON.parse(result);

        	for(var i=0;i<a.length;i++){

				var bus_name       = a[i].bus_name;
                var bus_number     = a[i].bus_number
                var from_place     = a[i].from_place;
                var to_place       = a[i].to_place;
                var date           = a[i].date;
                var seat_count1    = a[i].seat_count1;
                var seat_type_id1  = a[i].seat_type_id1;
                var ac_type_id    = a[i].ac_type_id;
                var rate1       = a[i].rate1;

			    var bus_list_container=document.getElementById("bus_list_container");

			    var MainDivContainer = document.createElement("DIV"); 
			    MainDivContainer.setAttribute("id","MainDivContainer");

                var ButtonContainer = document.createElement("BUTTON");
                ButtonContainer.setAttribute("class","D212_bus_list_button");
                ButtonContainer.setAttribute("id", bus_number);

			    var ButtonDivContainerLeft = document.createElement("DIV"); 
			    ButtonDivContainerLeft.setAttribute("id","ButtonDivContainerLeft");
			    ButtonDivContainerLeft.setAttribute("class","D212_Button_Div_Container_Left");

			    var ButtonDivContainerRight = document.createElement("DIV"); 
			    ButtonDivContainerRight.setAttribute("id","ButtonDivContainerRight");
			    ButtonDivContainerRight.setAttribute("class","D212_Button_Div_Container_Right");

			    var BusNameContainer = document.createElement("DIV"); 
			    BusNameContainer.setAttribute("id","BusNameContainer");
			    BusNameContainer.setAttribute("class","D213_Bus_Name_Container");
			    var BusNameContainerText = document.createTextNode(bus_name);
			    BusNameContainer.appendChild(BusNameContainerText);

                var BusNumberContainer = document.createElement("input"); 
                BusNumberContainer.setAttribute("id","BusNumberContainer"+i);
                BusNumberContainer.setAttribute("name","BusNumberContainer"+i);
                BusNumberContainer.setAttribute("value",bus_number);
                BusNumberContainer.setAttribute("style","visibility:hidden");

			    var FromAndToPlaceContainer = document.createElement("DIV"); 
			    FromAndToPlaceContainer.setAttribute("id","FromAndToPlaceContainer");
			    FromAndToPlaceContainer.setAttribute("class","D213_From_And_To_Place_Container");
			    var FromAndToPlaceContainerText = document.createTextNode(from_place+" to "+to_place);
			    FromAndToPlaceContainer.appendChild(FromAndToPlaceContainerText);

			    var SeatCountContainer = document.createElement("DIV"); 
			    SeatCountContainer.setAttribute("id","SeatCountContainer");
			    SeatCountContainer.setAttribute("class","D213_Seat_Count_Container");
			    var SeatCountContainerText =document.createTextNode(seat_count1);
			    SeatCountContainer.appendChild(SeatCountContainerText);

			    var AcTypeContainer  = document.createElement("DIV"); 
			    AcTypeContainer.setAttribute("id","AcTypeContainer");
			    AcTypeContainer.setAttribute("class","D213_Ac_Type_Container");
			    var AcTypeContainerText  = document.createTextNode(ac_type_id);
			    AcTypeContainer.appendChild(AcTypeContainerText);

			    var DateTimeContainer = document.createElement("DIV"); 
			    DateTimeContainer.setAttribute("id","DateTimeContainer");
			    DateTimeContainer.setAttribute("class","D213_Date_Time_Container");
			    var DateTimeContainerText =  document.createTextNode(date);
			    DateTimeContainer.appendChild(DateTimeContainerText);

			    var RateContainer = document.createElement("DIV");
			    RateContainer.setAttribute("id","RateContainer");
			    RateContainer.setAttribute("class","D213_Rate_Container");
			    var RateContainerText = document.createTextNode(rate1);
			    RateContainer.appendChild(RateContainerText); 

			    ButtonDivContainerLeft.appendChild(BusNameContainer);
			    ButtonDivContainerLeft.appendChild(FromAndToPlaceContainer);
			    ButtonDivContainerLeft.appendChild(SeatCountContainer);
			    ButtonDivContainerLeft.appendChild(AcTypeContainer);

			    ButtonDivContainerRight.appendChild(DateTimeContainer);
			    ButtonDivContainerRight.appendChild(RateContainer);
                ButtonDivContainerRight.appendChild(BusNumberContainer);

			    ButtonContainer.appendChild(ButtonDivContainerLeft);
			    ButtonContainer.appendChild(ButtonDivContainerRight);
                ButtonContainer.onclick = function() { 
                    nextUrlSendingValue(this.id)
                };

			    MainDivContainer.appendChild(ButtonContainer);
			    bus_list_container.appendChild(MainDivContainer);


		    }
	    }

        function BusSearchCommonreload(from_place,to_place,date_time){


            document.getElementById("SearchBus").style.display="none";
            document.getElementById("BusList").style.display="block";

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    resultofsearchbusreload(this.responseText)
                }
            }
            xmlHttp.open("post", "CommonBusSearch?from_place="+from_place+"&to_place="+to_place+"&date_time="+date_time);
            xmlHttp.send();
        }

        function resultofsearchbusreload(result){
            var a=JSON.parse(result);

            for(var i=0;i<a.length;i++){

                var bus_name       = a[i].bus_name;
                var bus_number     = a[i].bus_number
                var from_place     = a[i].from_place;
                var to_place       = a[i].to_place;
                var date           = a[i].date;
                var seat_count1    = a[i].seat_count1;
                var seat_type_id1  = a[i].seat_type_id1;
                var ac_type_id    = a[i].ac_type_id;
                var rate1       = a[i].rate1;

                var bus_list_container=document.getElementById("bus_list_container");

                var MainDivContainer = document.createElement("DIV"); 
                MainDivContainer.setAttribute("id","MainDivContainer");

                var ButtonContainer = document.createElement("BUTTON");
                ButtonContainer.setAttribute("class","D212_bus_list_button");
                ButtonContainer.setAttribute("id", bus_number);


                var ButtonDivContainerLeft = document.createElement("DIV"); 
                ButtonDivContainerLeft.setAttribute("id","ButtonDivContainerLeft");
                ButtonDivContainerLeft.setAttribute("class","D212_Button_Div_Container_Left");

                var ButtonDivContainerRight = document.createElement("DIV"); 
                ButtonDivContainerRight.setAttribute("id","ButtonDivContainerRight");
                ButtonDivContainerRight.setAttribute("class","D212_Button_Div_Container_Right");

                var BusNameContainer = document.createElement("DIV"); 
                BusNameContainer.setAttribute("id","BusNameContainer");
                BusNameContainer.setAttribute("class","D213_Bus_Name_Container");
                var BusNameContainerText = document.createTextNode(bus_name);
                BusNameContainer.appendChild(BusNameContainerText);

                var BusNumberContainer = document.createElement("input"); 
                BusNumberContainer.setAttribute("id","BusNumberContainer"+i);
                BusNumberContainer.setAttribute("name","BusNumberContainer"+i);
                BusNumberContainer.setAttribute("value",bus_number);
                BusNumberContainer.setAttribute("style","visibility:hidden");


                var FromAndToPlaceContainer = document.createElement("DIV"); 
                FromAndToPlaceContainer.setAttribute("id","FromAndToPlaceContainer");
                FromAndToPlaceContainer.setAttribute("class","D213_From_And_To_Place_Container");
                var FromAndToPlaceContainerText = document.createTextNode(from_place+" to "+to_place);
                FromAndToPlaceContainer.appendChild(FromAndToPlaceContainerText);

                var SeatCountContainer = document.createElement("DIV"); 
                SeatCountContainer.setAttribute("id","SeatCountContainer");
                SeatCountContainer.setAttribute("class","D213_Seat_Count_Container");
                var SeatCountContainerText =document.createTextNode(seat_count1);
                SeatCountContainer.appendChild(SeatCountContainerText);

                var AcTypeContainer  = document.createElement("DIV"); 
                AcTypeContainer.setAttribute("id","AcTypeContainer");
                AcTypeContainer.setAttribute("class","D213_Ac_Type_Container");
                var AcTypeContainerText  = document.createTextNode(ac_type_id);
                AcTypeContainer.appendChild(AcTypeContainerText);

                var DateTimeContainer = document.createElement("DIV"); 
                DateTimeContainer.setAttribute("id","DateTimeContainer");
                DateTimeContainer.setAttribute("class","D213_Date_Time_Container");
                var DateTimeContainerText =  document.createTextNode(date);
                DateTimeContainer.appendChild(DateTimeContainerText);

                var RateContainer = document.createElement("DIV");
                RateContainer.setAttribute("id","RateContainer");
                RateContainer.setAttribute("class","D213_Rate_Container");
                var RateContainerText = document.createTextNode(rate1);
                RateContainer.appendChild(RateContainerText); 

                ButtonDivContainerLeft.appendChild(BusNameContainer);
                ButtonDivContainerLeft.appendChild(FromAndToPlaceContainer);
                ButtonDivContainerLeft.appendChild(SeatCountContainer);
                ButtonDivContainerLeft.appendChild(AcTypeContainer);

                ButtonDivContainerRight.appendChild(DateTimeContainer);
                ButtonDivContainerRight.appendChild(RateContainer);
                ButtonDivContainerRight.appendChild(BusNumberContainer);

                ButtonContainer.appendChild(ButtonDivContainerLeft);
                ButtonContainer.appendChild(ButtonDivContainerRight);
                ButtonContainer.onclick = function() { 
                    
                    nextUrlSendingValue(this.id)
                };

                MainDivContainer.appendChild(ButtonContainer);
                bus_list_container.appendChild(MainDivContainer);


            }
        }

        function nextUrlSendingValue(BusNumberContainer){

            console.log(BusNumberContainer);

            window.location.href = "http://localhost:8010/BusReservation/CommonTicketBooking.jsp?BusNumber="+BusNumberContainer; 
        }

	    function urlappendingprocess(div_id,from_place,to_place,date_time){
	    	
				var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?div_id='+div_id+'&from_place='+from_place+'&to_place='+to_place+'&date_time='+date_time;
				window.history.pushState({ path: newurl }, '', newurl);
		
	    }


        function SearchBusDisplay(){
            document.getElementById("SearchBus").style.display="block";
            document.getElementById("BusList").style.display="none";
        }

       
        function firstfunction(){
            var url_string =window.location.href;
            var url = new URL(url_string);
            var div_id = url.searchParams.get("div_id");
            var from_place = url.searchParams.get("from_place");
            var to_place   = url.searchParams.get("to_place");
            var date_time  = url.searchParams.get("date_time");
            
            if (div_id=="BusList") {
                BusSearchCommonreload(from_place,to_place,date_time);
            }

            return div_id;
        }

    	function allfunctions(){
            ShowFromPlace()
            ShowToPlace()
            var div_id = firstfunction()

            if (div_id=="BusList") {
               
            }else if (div_id==null) {
                SearchBusDisplay()

            }
    		
    		
    	}


        function InsertDatacreatepassanger(){

            var name = encodeURIComponent(document.getElementById("username").value);
            var mail = encodeURIComponent(document.getElementById("mail").value);
            var phone_no = encodeURIComponent(document.getElementById("phone_no").value);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function()
            {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
                {
                    createpassangeraccountresult(this.responseText)
                }
            }
            xmlHttp.open("post", "CreateAccountPassanger?name="+name+"&mail="+mail+"&phone_no="+phone_no);
            xmlHttp.send();
        }

        function createpassangeraccountresult(result){
            var a = JSON.parse(result);

            var b = a.STATUS;

            console.log("karthi"+b);

            if (b=="SUCCESS") {
                alert("Account Successfully Created")
            }else if (b=="NAME_INCORRECT") {
                alert("Please Enter Correct UserName")
            }else if (b=="PHONENO_INCORRECT") {
                alert("Please Enter Correct Phone Number")
            }else if (b=="MAIL_INCORRECT") {
                alert("Please Enter Correct Mail Id")
            }else if (b=="WRONG_MAIL") {
                alert("Mail Id Not Available")
            }else if (b=="MAIL_NOT_AVAILABLE") {
                alert("Some technical fault...")
            }else if (b=="WRONG_MAIL") {
                alert("Please Enter Correct Mail Id")
            }
        }

        var modal = document.getElementById("myModal");

        var btn = document.getElementById("singninupbutton");

        var span = document.getElementsByClassName("close")[0];

        btn.onclick = function() {
            modal.style.display = "block";
        }

        span.onclick = function() {
            modal.style.display = "none";
        }

    </script>

</body>
</html>