<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bus Reservation</title>
<style type="text/css">
	.D112_signin_boxcontainer{
		width: 400px;
		height: 400px;
		margin-top: 200px;
		margin-left: 620px;
		border-bottom: 1px solid #3c3c3d87;
		border-top: 1px solid #3c3c3d87;
		border-left: 1px solid #3c3c3d87;
		border-right: 1px solid #3c3c3d87;
		border-radius: 10px;
		padding: 10px;
		display: inline-block;
	}
	.D112_sign_back_button {
		background-color: #d84f57;
		width: 30px;
		height: 30px;
		border-radius: 50px;
		text-align: center;
		display: block;
		margin-left: 370px;
	}
	.D112_input_divcontainer{
		display: block;
	}
	.D112_input_valuecontainer{
		width: 150px;
		padding: 5px;
		margin-top: 20px;
		margin-left: 110px;
		border-radius: 10px;
		display: block;
	}

	.D112_login_button{
		margin-top: 30px;
		width: 100px;
		margin-left: 140px;
		border-radius: 10px;
		display: block;
	}

	.D112_signup_call_button{
		margin-top: 100px;
		width: 300px;
		margin-left: 80px;
		padding: 10px;
		border-radius: 10px;
		display: block;
	}

	.D114_signup_boxcontainer{
		width: 400px;
		height: 400px;
		margin-top: 200px;
		margin-left: 620px;
		border-bottom: 1px solid #3c3c3d87;
		border-top: 1px solid #3c3c3d87;
		border-left: 1px solid #3c3c3d87;
		border-right: 1px solid #3c3c3d87;
		border-radius: 10px;
		padding: 10px;
		display: inline-block;
	}
	.D114_input_divcontainer{
		display: block;
	}
	.D114_input_valuecontainer{
		width: 150px;
		padding: 5px;
		margin-top: 20px;
		margin-left: 110px;
		border-radius: 10px;
		display: block;
	}

	.D114_signup_button{
		margin-top: 30px;
		width: 100px;
		margin-left: 140px;
		border-radius: 10px;
		display: block;
	}
</style>
</head>
<body onload="allfunctions()">


	<div class="D112_signin_boxcontainer" id="D112_signin_boxcontainer">
		
		<div class="D112_input_divcontainer">
			<input class="D112_sign_back_button" type="button" onclick="location.href='CommonPassanger.jsp';" value=" X " />
			<form action="LogIn" method="post">
			<input class="D112_input_valuecontainer" type="text" name="userName" id="userName" placeholder="User Name">
			<input class="D112_input_valuecontainer" type="password" name="password" id="password" placeholder="Password">
			<input class="D112_login_button" type="submit" name="submit" value="Log In">
			</form>
			<button class="D112_signup_call_button" onclick="Signupcallbutton()"> Create Your Account </button>
		</div>
	</div>
	


	<div class="D114_signup_boxcontainer" id="D114_signup_boxcontainer">
		<div class="D114_input_divcontainer">
			<button class="D112_sign_back_button" onclick="SignUpDivBackFun()"> X </button>
			<input class="D114_input_valuecontainer" type="text" name="username" id = "username" placeholder="User Name">
			<input class="D114_input_valuecontainer" type="text" name="mail" id ="mail" placeholder="Mail">
			<input class="D114_input_valuecontainer" type="text" name="phone_no" id ="phone_no" placeholder="Phone No ">
			<input class="D114_signup_button" type="submit" value="submit" onclick="InsertDatacreatepassanger()">
		</div>
	</div>	
	
    <script type="text/javascript">
    
    
   
    
   
    	function SignUpDivBackFun(){
    		document.getElementById("D114_signup_boxcontainer").style.display = "none";
    		document.getElementById("D112_signin_boxcontainer").style.display = "block";
    	}

    	function Signupcallbutton(){
    		document.getElementById("D112_signin_boxcontainer").style.display = "none";
    		document.getElementById("D114_signup_boxcontainer").style.display = "block";

    	}

    	function InsertDatacreatepassanger(){

    		var name = encodeURIComponent(document.getElementById("username").value);
    		var mail = encodeURIComponent(document.getElementById("mail").value);
    		var phone_no = encodeURIComponent(document.getElementById("phone_no").value);

    		var xmlHttp = new XMLHttpRequest();
    		xmlHttp.onreadystatechange = function()
    		{
    			if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
    			{
    				createpassangeraccountresult(this.responseText)
    			}
            }
            xmlHttp.open("post", "CreateAccountPassanger?name="+name+"&mail="+mail+"&phone_no="+phone_no);
            xmlHttp.send();
        }

        function createpassangeraccountresult(result){
        	var a = JSON.parse(result);

        	var b = a.status;

        	if (b=="SUCCESS") {
        		alert("Account Successfully Created")
        	}else if (b=="PHONENOAVAILABLE") {
        		alert("Phone Number is already registered plz.. enter any other number")
        	}else if (b=="MAILAVAILABLE") {
        		alert("Mail Id is already registered plz.. enter any other mail id")
        	}else if (b=="USERNAMEAVAILABLE") {
        		alert("User Name is already registered plz.. enter any other user name")
        	}else if (b=="Failure") {
        		alert("Some technical fault...")
        	}
        }

        function alldisplayfunction(){
        	document.getElementById("D114_signup_boxcontainer").style.display ="none"
        }
       

        function allfunctions(){
        	alldisplayfunction()
        }
        
       
    	
    </script>

</body>
</html>