package commonPassangerActions;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utility.DatabaseConnection;

public class TicketAvailable {
	
	String trip_id;

	
	public void execute(){
		
		try {
			
			TicketAvailable ticketAvailable = new TicketAvailable();
			ticketAvailable.setTrip_id(trip_id);
			
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
		    PrintWriter writer = response.getWriter();
		    writer.write(ticketAvaialble(ticketAvailable).toString());
		    writer.flush();
		    writer.close();
			
		} catch (Exception e) {
			
		}
		
		
		
	}
	
	public JSONArray ticketAvaialble(TicketAvailable ticketAvailable) throws SQLException{
		
		JSONArray  jsonArray  = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		 
		Connection conn = DatabaseConnection.getConnection();
			 
		PreparedStatement FetchTicketAvailable = conn.prepareStatement("select * from seat_details where trip_id = ?");
		FetchTicketAvailable.setString(1, ticketAvailable.getTrip_id());
		ResultSet FetchTicketAvailableResult = FetchTicketAvailable.executeQuery();
			 
		while(FetchTicketAvailableResult.next()){
			
			jsonObject.put("seat_number",FetchTicketAvailableResult.getString("seat_number"));
			jsonObject.put("status", FetchTicketAvailableResult.getInt("status"));
			jsonArray.add(jsonObject);
		}
		
		return jsonArray;
		
	}

	public String getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}
	
	
	

}
