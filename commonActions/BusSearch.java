package commonPassangerActions;


import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class BusSearch {
	
	
	String from_place;
	String to_place;
	String date_time;
	
	
	public void busSearch(){
		
		try {
			
			BusSearch ticketBooking = new BusSearch();
			ticketBooking.setFrom_place(from_place);
			ticketBooking.setTo_place(to_place);
			ticketBooking.setDate_time(date_time);

			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
		    PrintWriter writer = response.getWriter();
		    writer.write(ticketBooking(ticketBooking).toString());
		    writer.flush();
		    writer.close();
			 
			
		} catch (Exception e) {
			
		}
		
		
		
	}
	
	public JSONArray ticketBooking(BusSearch ticketBooking) throws SQLException{
		
		
		JSONArray jsonArray   = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		Connection conn = DatabaseConnection.getConnection();
			 
		PreparedStatement BusSearchList = conn.prepareStatement(ConstantValues.FETCH_DATA_BUS_SEARCH);
		BusSearchList.setString(1, ticketBooking.getDate_time());
		BusSearchList.setString(2, ticketBooking.getFrom_place());
		BusSearchList.setString(3, ticketBooking.getTo_place());
		ResultSet BusSearchListResult = BusSearchList.executeQuery();
		
		while (BusSearchListResult.next()) {
			
			
			jsonObject.put("rate1",BusSearchListResult.getInt("rate1"));
			jsonObject.put("rate2",BusSearchListResult.getInt("rate2"));
			jsonObject.put("date",BusSearchListResult.getString("date"));
			jsonObject.put("trip_id", BusSearchListResult.getString("trip_id"));
			jsonObject.put("ac_type", BusSearchListResult.getString("ac_type"));
			jsonObject.put("to_place", BusSearchListResult.getString("to_place"));
			jsonObject.put("seat_type1",BusSearchListResult.getString("seat_type1"));
			jsonObject.put("seat_type2", BusSearchListResult.getString("seat_type2"));
			jsonObject.put("from_place", BusSearchListResult.getString("from_place"));
			jsonObject.put("seat_count1",BusSearchListResult.getString("seat_count1"));
			jsonObject.put("seat_count2",BusSearchListResult.getString("seat_count2"));
			jsonObject.put("arrival_time",BusSearchListResult.getString("arrival_time"));
			jsonObject.put("travels_name", BusSearchListResult.getString("travels_name"));
			jsonObject.put("departure_time",BusSearchListResult.getString("departure_time"));
			
			jsonArray.add(jsonObject);
			
		}
		
		return jsonArray;
	}
	
	
	public String getFrom_place() {
		return from_place;
	}
	public void setFrom_place(String from_place) {
		this.from_place = from_place;
	}
	public String getTo_place() {
		return to_place;
	}
	public void setTo_place(String to_place) {
		this.to_place = to_place;
	}
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	

}
