package commonPassangerActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class ShowToPlace {

	public void showToPlace(){
		
		try {
			JSONArray jsonArray = showToPlaceProcess();
			
			 HttpServletResponse response = ServletActionContext.getResponse();
			 response.setContentType("application/json");
			 response.setCharacterEncoding("UTF-8");
		     PrintWriter writer = response.getWriter();
		     writer.write(jsonArray.toString());
		     writer.flush();
		     writer.close();
			
		} catch (ClassNotFoundException | SQLException | IOException e) {
			
		} 
		
	}
	
	
	public JSONArray showToPlaceProcess() throws ClassNotFoundException, SQLException{
		
		ArrayList<String> ToList = new ArrayList<String>();
		JSONObject to_placeList = new JSONObject();
		JSONArray list1 = new JSONArray();
		String to_place = null;
		
		Connection conn =DatabaseConnection.getConnection();
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(ConstantValues.DATE_FROMAT);  
		LocalDateTime now = LocalDateTime.now();  
		String date = dtf.format(now);  
		
		PreparedStatement stm = conn.prepareStatement(ConstantValues.FETCH_QUERY_TO_PLACE);
		stm.setString(1, date);
	    ResultSet rs = stm.executeQuery();

	        while (rs.next()) {

	        	to_place = rs.getString(ConstantValues.TO_PLACE);
		    	
		    	if (ToList.size() == 0) {
		    		ToList.add(to_place);
		    	}else{
		    		for (int i =0 ; i<ToList.size();i++){
		    			
		    			if (ToList.get(i).equals(to_place)){
		    				
		    			}else{
		    				ToList.add(to_place);
		    			}
		    		}
		    	}
	        	
	        	}
	        
		    for (int i =0 ; i<ToList.size();i++){
		    	to_placeList.put(ConstantValues.TO_PLACE, ToList.get(i));
		    	list1.add(to_placeList);
		    }
		       
	        
	       
	   return list1;
	 
	}
}
