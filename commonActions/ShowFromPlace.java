package commonPassangerActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class ShowFromPlace {
	
	public void showFromPlace(){
		
		try {
			JSONArray jsonArray = showFromPlaceProcess();
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.write(jsonArray.toString());
			writer.flush();
			writer.close();
			
		} catch (ClassNotFoundException | SQLException | IOException e) {
				
		} 
		
	}
	
	
	public JSONArray showFromPlaceProcess() throws ClassNotFoundException, SQLException{
		
		ArrayList<String> FromList = new ArrayList<String>();
		JSONObject from_placeList = new JSONObject();
		JSONArray list1 = new JSONArray();
		String from_place = null;
		
		Connection conn = DatabaseConnection.getConnection();
		

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(ConstantValues.DATE_FROMAT);  
		LocalDateTime now = LocalDateTime.now();  
		String date = dtf.format(now);  
		
		PreparedStatement FetchFromPlace = conn.prepareStatement(ConstantValues.FETCH_QUERY_FROM_PLACE);
		FetchFromPlace.setString(1, date);
	    ResultSet FetchFromPlaceResult = FetchFromPlace.executeQuery();
	    
	    while (FetchFromPlaceResult.next()) {
	    	
	    	from_place = FetchFromPlaceResult.getString(ConstantValues.FROM_PLACE);
	    	
	    	if (FromList.size() == 0) {
	    		FromList.add(from_place);
	    	}else{
	    		for (int i =0 ; i<FromList.size();i++){
	    			
	    			if (FromList.get(i).equals(from_place)){
	    				
	    			}else{
	    				FromList.add(from_place);
	    			}
	    		}
	    	}
	    }
	    
	    for (int i =0 ; i<FromList.size();i++){
	    	from_placeList.put(ConstantValues.FROM_PLACE, FromList.get(i));
	    	list1.add(from_placeList);
	    }
	       
	       
	   return list1;
	 
	}

}
