package adminActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class FetchBusByDate {
	
	String date;
	String admin_user;
	
	
	public void fetchBusByDate(){
		
		
		try {
			 Map<String, Object> session = ActionContext.getContext().getSession();
			 FetchBusByDate fetchBusByDate = new FetchBusByDate();
			 fetchBusByDate.setDate(date);
			 fetchBusByDate.setAdmin_user((String) session.get("userName"));
			 
			 
			 HttpServletResponse response = ServletActionContext.getResponse();
			 response.setContentType("application/json");
			 response.setCharacterEncoding("UTF-8");
			 PrintWriter writer = response.getWriter();
			 writer.write(showFetchBusByDate(fetchBusByDate).toString());
			 writer.flush();
			 writer.close();
				
			
		} catch (ClassNotFoundException | SQLException | IOException e) {
			
		} 
		
	}
	
	
	public JSONArray showFetchBusByDate(FetchBusByDate fetchBusByDate) throws ClassNotFoundException, SQLException{
		
		JSONObject showBusList = new JSONObject();
		JSONArray list1 = new JSONArray();
		
		Connection conn = DatabaseConnection.getConnection();
			
		PreparedStatement FetchBusByDate = conn.prepareStatement(ConstantValues.FETCH_BUS_BY_DATE);
		FetchBusByDate.setString(1, fetchBusByDate.getDate());
		ResultSet FetchBusByDateResult = FetchBusByDate.executeQuery();
		   
		
		while (FetchBusByDateResult.next()) {
			
			showBusList.put("date", FetchBusByDateResult.getString("date"));
			showBusList.put("trip_id", FetchBusByDateResult.getInt("trip_id"));
			showBusList.put("travels_name", FetchBusByDateResult.getString("travels_name"));
			showBusList.put("bus_number", FetchBusByDateResult.getString("bus_number"));
			showBusList.put("from_place",FetchBusByDateResult.getString("from_place"));
        	showBusList.put("to_place", FetchBusByDateResult.getString("to_place"));
        	showBusList.put("arrival_time", FetchBusByDateResult.getString("arrival_time"));
        	showBusList.put("departure_time", FetchBusByDateResult.getString("departure_time"));
        	showBusList.put("seat_count1", FetchBusByDateResult.getInt("seat_count1"));
        	showBusList.put("seat_count2", FetchBusByDateResult.getInt("seat_count2"));
        	showBusList.put("seat_type1", FetchBusByDateResult.getString("seat_type1"));
        	showBusList.put("seat_type2", FetchBusByDateResult.getString("seat_type2"));
        	showBusList.put("ac_type", FetchBusByDateResult.getString("ac_type"));
        	showBusList.put("rate1", FetchBusByDateResult.getInt("rate1"));
        	showBusList.put("rate2", FetchBusByDateResult.getInt("rate2"));
        	
        	list1.add(showBusList);
        	
		}
		
		return list1;
	
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getAdmin_user() {
		return admin_user;
	}


	public void setAdmin_user(String admin_user) {
		this.admin_user = admin_user;
	}
	
	
	

}
