package adminActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import net.sf.json.JSONObject;
import utility.AddTripConstructor;
import utility.ConstantValues;
import utility.DatabaseConnection;
import validationActions.AddTripValidation;

public class AddTripMultiple {

	
	String bus_number;
	String trip_name;
	String from_place;
	String to_place;
	String date;
	String arrival_time;
	String departure_time;
	String seat_count1;
	String seat_count2;
	String seat_type1;
	String seat_type2;
	String ac_type;
	String rate1;
	String rate2;
	
	
	public void addTripMultiple(){
		
		try {
			HashMap<String, String> validation = new HashMap<String, String>();
			AddTripValidation addTripValidation = new AddTripValidation();
			JSONObject json = new JSONObject();
			AddTripMultiple addTrip = new AddTripMultiple();
			String addTripValue;
			addTrip.setBus_number(bus_number);
			addTrip.setTrip_name(trip_name);
			addTrip.setFrom_place(from_place);
			addTrip.setTo_place(to_place);
			addTrip.setDate(date);
			addTrip.setArrival_time(arrival_time);
			addTrip.setDeparture_time(departure_time);
			addTrip.setSeat_count1(seat_count1);
			addTrip.setSeat_count2(seat_count2);
			addTrip.setSeat_type1(seat_type1);
			addTrip.setSeat_type2(seat_type2);
			addTrip.setAc_type(ac_type);
			addTrip.setRate1(rate1);
			addTrip.setRate2(rate2);
			
			validation = addTripValidation.addTripMultipleValdiation(addTrip);
			
			if (validation.get(ConstantValues.STATUS).equals(ConstantValues.SUCCESS)) {
				
				addTripValue = addTripRegister(addTrip);
				
				if (addTripValue.equals(ConstantValues.AVAILABLE)) {
					json.put(ConstantValues.STATUS, ConstantValues.SUCCESS);
				}else{
					json.put(ConstantValues.STATUS, addTripValue);
				}
			}else{
				json.put(ConstantValues.STATUS, validation.get(ConstantValues.STATUS));
			}
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
			writer.flush();
			writer.close();
			
		} catch (SQLException | IOException e) {
			
			
		}
		
	}

	public String addTripRegister(AddTripMultiple addTrip) throws SQLException{
		
		List <AddTripConstructor > list = new ArrayList < > ();
		Connection dataConnection = DatabaseConnection.getConnection();
		int bus_id = 0;
		int trip_id = 0;
		
		PreparedStatement FetchBusNumber = dataConnection.prepareStatement(ConstantValues.FETCH_BUSID_FORM_BUS_DETAILS);
		FetchBusNumber.setString(1, addTrip.getBus_number());
        ResultSet FetchBusNumberResult = FetchBusNumber.executeQuery();

        while (FetchBusNumberResult.next()) {
        	
        	bus_id = FetchBusNumberResult.getInt("bus_id");

        }
        
        String tripAvailable = tripavailable(addTrip, bus_id);
        
        if (tripAvailable.equals(ConstantValues.AVAILABLE)) {
        	
        	PreparedStatement FetchMaxTripId = dataConnection.prepareStatement(ConstantValues.FETCH_MAX_TRIP_ID);
			ResultSet FetchMaxTripIdResult = FetchMaxTripId.executeQuery();
			
			while (FetchMaxTripIdResult.next()) {
				
				trip_id = FetchMaxTripIdResult.getInt("maxId");
				
			}
			
			trip_id = trip_id + 1;
			
        	PreparedStatement InsertIntoTripDetails = dataConnection.prepareStatement(ConstantValues.INSERT_INTO_MULTIPLE_TRIP_DETAILS);
     		InsertIntoTripDetails.setString(1,addTrip.getTrip_name());
     		InsertIntoTripDetails.setString(2,addTrip.getFrom_place());
     		InsertIntoTripDetails.setString(3,addTrip.getTo_place());
     		InsertIntoTripDetails.setString(4,addTrip.getArrival_time());
     		InsertIntoTripDetails.setString(5,addTrip.getDeparture_time());
     		InsertIntoTripDetails.setInt(6,Integer.parseInt(addTrip.getSeat_count1()));
     		InsertIntoTripDetails.setString(7,addTrip.getSeat_type1());
     		InsertIntoTripDetails.setString(8,addTrip.getAc_type());
     		InsertIntoTripDetails.setInt(9,Integer.parseInt(addTrip.getRate1()));
     		InsertIntoTripDetails.setInt(10,bus_id);
     		InsertIntoTripDetails.setString(11,addTrip.getDate());
     		InsertIntoTripDetails.setInt(12,Integer.parseInt(addTrip.getSeat_count2()));
     		InsertIntoTripDetails.setString(13,addTrip.getSeat_type2());
     		InsertIntoTripDetails.setInt(14,Integer.parseInt(addTrip.getRate2()));
     		InsertIntoTripDetails.executeUpdate();
     		
     		int seatCount = Integer.parseInt(addTrip.getSeat_count1())+Integer.parseInt(addTrip.getSeat_count2());
     		
     		System.out.println(seatCount);
     		
     		for (int i = 1; i <= seatCount; i++) {
     			
     			list.add(new AddTripConstructor("S"+i,trip_id));
			}
     		

	        PreparedStatement InsertIntoSeatDetails = dataConnection.prepareStatement(ConstantValues.INSERT_INTO_SEAT_DETAILS);
	        
	        for (Iterator < AddTripConstructor > iterator = list.iterator(); iterator.hasNext();) {
	        	AddTripConstructor user = (AddTripConstructor) iterator.next();
	        	InsertIntoSeatDetails.setString(1, user.getSeat_number());
	        	InsertIntoSeatDetails.setInt(2, user.getTrip_id());
	        	InsertIntoSeatDetails.setInt(3, 0);
	        	InsertIntoSeatDetails.addBatch();
	        	InsertIntoSeatDetails.executeBatch();
              
	        }
     		
     		return tripAvailable;
		}else{
			
			return tripAvailable;
		}
       
	}
	
	
	public String tripavailable(AddTripMultiple addTrip,int bus_id) throws NumberFormatException, SQLException{
		
		Connection dataConnection = DatabaseConnection.getConnection();
        ArrayList<String> list = new ArrayList<String>();
        int timeDifference;
        String date;
        String arrival_time;
        String departure_time;

        PreparedStatement FetchDateDetails = dataConnection.prepareStatement(ConstantValues.FETCH_DATE_DETAILS_TRIP_DETAILS);
        FetchDateDetails.setInt(1, bus_id);
        ResultSet FetchDateDetailsResult = FetchDateDetails.executeQuery();

        while (FetchDateDetailsResult.next()) {

            date           = FetchDateDetailsResult.getString("date");
            arrival_time   = FetchDateDetailsResult.getString("arrival_time");
            departure_time = FetchDateDetailsResult.getString("departure_time");

            if (date.equals(addTrip.getDate())) {

                timeDifference = Math.abs(Integer.parseInt(arrival_time.substring(0,2))-Integer.parseInt(departure_time.substring(0,2)));

                if ( Integer.parseInt(addTrip.getArrival_time().substring(0,2)) < Integer.parseInt(arrival_time.substring(0,2))+timeDifference) {

                    list.add(ConstantValues.NOT_AVAILABLE);
                }else{
                   list.add(ConstantValues.AVAILABLE);
                }
            }
        }
        
        for (int i =0 ; i<list.size();i++){
        	
            if (list.get(i).equals(ConstantValues.NOT_AVAILABLE)){
                return ConstantValues.NOT_AVAILABLE;
            }
            
        }

        return ConstantValues.AVAILABLE;

	}
	
	
	
	
	public String getBus_number() {
		return bus_number;
	}
	public void setBus_number(String bus_number) {
		this.bus_number = bus_number;
	}
	public String getTrip_name() {
		return trip_name;
	}
	public void setTrip_name(String trip_name) {
		this.trip_name = trip_name;
	}
	public String getFrom_place() {
		return from_place;
	}
	public void setFrom_place(String from_place) {
		this.from_place = from_place;
	}
	public String getTo_place() {
		return to_place;
	}
	public void setTo_place(String to_place) {
		this.to_place = to_place;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getArrival_time() {
		return arrival_time;
	}
	public void setArrival_time(String arrival_time) {
		this.arrival_time = arrival_time;
	}
	public String getDeparture_time() {
		return departure_time;
	}
	public void setDeparture_time(String departure_time) {
		this.departure_time = departure_time;
	}
	public String getSeat_count1() {
		return seat_count1;
	}
	public void setSeat_count1(String seat_count1) {
		this.seat_count1 = seat_count1;
	}
	public String getSeat_count2() {
		return seat_count2;
	}
	public void setSeat_count2(String seat_count2) {
		this.seat_count2 = seat_count2;
	}
	public String getSeat_type1() {
		return seat_type1;
	}
	public void setSeat_type1(String seat_type1) {
		this.seat_type1 = seat_type1;
	}
	public String getSeat_type2() {
		return seat_type2;
	}
	public void setSeat_type2(String seat_type2) {
		this.seat_type2 = seat_type2;
	}
	public String getAc_type() {
		return ac_type;
	}
	public void setAc_type(String ac_type) {
		this.ac_type = ac_type;
	}
	public String getRate1() {
		return rate1;
	}
	public void setRate1(String rate1) {
		this.rate1 = rate1;
	}
	public String getRate2() {
		return rate2;
	}
	public void setRate2(String rate2) {
		this.rate2 = rate2;
	}
	
	
}
