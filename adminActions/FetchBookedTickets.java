package adminActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class FetchBookedTickets {
	
	String trip_id;
	
	
	public void fetchBookedTickets(){
		
		try {
			
			FetchBookedTickets fetchBookedTickets = new FetchBookedTickets();
			fetchBookedTickets.setTrip_id(trip_id);
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.write(showBusList(fetchBookedTickets).toString());
			writer.flush();
			writer.close();
					
				
		} catch (ClassNotFoundException | SQLException | IOException e) {
			
		} 
		
	}
	
	
	public JSONArray showBusList(FetchBookedTickets fetchBookedTickets) throws ClassNotFoundException, SQLException{
		
		JSONObject showBusList = new JSONObject();
		JSONArray list1 = new JSONArray();
		
		
		Connection conn = DatabaseConnection.getConnection();
		
		PreparedStatement FetchBookedTickets = conn.prepareStatement(ConstantValues.FETCH_BOOKED_TICKETS);
		FetchBookedTickets.setString(1, fetchBookedTickets.getTrip_id());
		ResultSet FetchBookedTicketsResult = FetchBookedTickets.executeQuery();
		
		while (FetchBookedTicketsResult.next()) {
			
			showBusList.put("seat_number", FetchBookedTicketsResult.getString("seat_number"));
			showBusList.put("name", FetchBookedTicketsResult.getString("name"));
			showBusList.put("mail", FetchBookedTicketsResult.getString("mail"));
		    showBusList.put("phone_no", FetchBookedTicketsResult.getString("phone_no"));
		    showBusList.put("gender", FetchBookedTicketsResult.getString("gender"));
		    showBusList.put("status", FetchBookedTicketsResult.getInt("status"));
		    
		    list1.add(showBusList);
		    
		} 
		
		return list1;
	 
	}

	public String getTrip_id() {
		return trip_id;
	}


	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}


	
	
}
