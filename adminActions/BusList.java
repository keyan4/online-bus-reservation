package adminActions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;

public class BusList {
	
	String bus_name;
	String bus_number;
	String admin_user;
	
	
	public void busList(){
		
		try {
			
			Map<String, Object> session = ActionContext.getContext().getSession();
			admin_user = (String) session.get("userName");
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
		    PrintWriter writer = response.getWriter();
		    writer.write(showBusList(admin_user).toString());
		    writer.flush();
		    writer.close();
					
			
		} catch (ClassNotFoundException | SQLException | IOException e) {
			
		} 
		
	}
	
	
	public JSONArray showBusList(String admin_user) throws ClassNotFoundException, SQLException{
		
		JSONObject showBusList = new JSONObject();
		JSONArray list1 = new JSONArray();
		Connection conn = DatabaseConnection.getConnection();
		
		PreparedStatement FetchBusList = conn.prepareStatement(ConstantValues.FETCH_BUSLIST);
		FetchBusList.setInt(1, Integer.parseInt(admin_user));
		ResultSet FetchBusListresult = FetchBusList.executeQuery();
		
		while (FetchBusListresult.next()) {
			
			showBusList.put("bus_name", FetchBusListresult.getString("travels_name"));
			showBusList.put("bus_number", FetchBusListresult.getString("bus_number"));
			
			list1.add(showBusList);
			
		}
		
		return list1;
	 
	}
	
}
