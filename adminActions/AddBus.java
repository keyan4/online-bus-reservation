package adminActions;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;
import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;
import validationActions.AddBusValidation;

public class AddBus {

	String bus_number;
	String travels_name;
	
	
	public void addBus(){
		
		try {
			HashMap<String, String> validation = new HashMap<String, String>();
			AddBusValidation addBusValidation = new AddBusValidation();
			JSONObject json = new JSONObject();
			AddBus addBus = new AddBus();
			
			addBus.setBus_number(bus_number);
			addBus.setTravels_name(travels_name);
			
			validation = addBusValidation.addBusValidation(addBus);
			
			if (validation.get(ConstantValues.STATUS).equals(ConstantValues.SUCCESS)) {
				
				if ( busreservation(addBus)) {
					json.put(ConstantValues.STATUS, ConstantValues.SUCCESS);
				}else{
					json.put(ConstantValues.STATUS, ConstantValues.FAILURE);
				}
			}else{
				json.put(ConstantValues.STATUS, validation.get(ConstantValues.STATUS));
			}
			
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.write(json.toString());
			writer.flush();
			writer.close();
					
		} catch (Exception e) {
			
		}	
	}
	
	public boolean busreservation(AddBus addBus) throws SQLException{
	
			Connection dataConnection = DatabaseConnection.getConnection();
			
			PreparedStatement InsertIntoBusDetails = dataConnection.prepareStatement(ConstantValues.INSERT_INTO_BUSDETAILS);
			InsertIntoBusDetails.setString(1,addBus.getBus_number());
			InsertIntoBusDetails.setString(2,addBus.getTravels_name());
		    int i = InsertIntoBusDetails.executeUpdate();
		    
		    if (i == 1) {
		    	return true;
			}else{
				return false;
			}
			
		
	}

	
	public String getBus_number() {
		return bus_number;
	}
	public void setBus_number(String bus_number) {
		this.bus_number = bus_number;
	}
	public String getTravels_name() {
		return travels_name;
	}
	public void setTravels_name(String travels_name) {
		this.travels_name = travels_name;
	}
	
	
}
