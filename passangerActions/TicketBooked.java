package passangerActions;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONObject;
import utility.ConstantValues;
import utility.DatabaseConnection;
import validationActions.TicketBookingValidation;

public class TicketBooked {

	
	String trip_id;
	String card_number;
	String card_exp;
	String ccv_number;
	String name;
	String mail;
	String phone_no;
	String gender;
	String seat_id;
	String save_card;
	
	public void ticketBooked(){
		
		try {
			TicketBookingValidation ticketBookingValidation = new TicketBookingValidation();
			HashMap<String, String> validation = new HashMap<String,String>();
			JSONObject  jsonObject = new JSONObject();
			TicketBooked ticketBooked  = new TicketBooked();
			ticketBooked.setTrip_id(trip_id);;
			ticketBooked.setName(name);
			ticketBooked.setMail(mail);
			ticketBooked.setPhone_no(phone_no);
			ticketBooked.setGender(gender);
			ticketBooked.setCard_number(card_number);
			ticketBooked.setCcv_number(ccv_number);
			ticketBooked.setSeat_id(seat_id);
			ticketBooked.setSave_card(save_card);
			
			 Map<String, Object> session = ActionContext.getContext().getSession();
			 
			 String passanger_id = (String) session.get("userName");
			
	         validation = ticketBookingValidation.passangerticketBooking(ticketBooked);
			

			String status = validation.get("status");
		
			 if (status.equals(ConstantValues.SUCCESS)) {
				jsonObject  = ticketBooked(ticketBooked,passanger_id);
			 }else{
				 jsonObject.put(ConstantValues.STATUS, status);
			 }    
			

			HttpServletResponse response = ServletActionContext.getResponse();
	        response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        PrintWriter writer = response.getWriter();
	        writer.write(jsonObject.toString());
	        writer.flush();
	        writer.close();
		} catch (Exception e) {
			
		}
		
		
			
		
	}
	
	
	public JSONObject ticketBooked(TicketBooked ticketBooked, String passanger_id) throws NumberFormatException, SQLException{
		
		JSONObject json = new JSONObject();
		Connection conn = DatabaseConnection.getConnection();
		
		PreparedStatement TicketBooked = conn.prepareStatement(ConstantValues.TICKET_BOOKED_PASSANGER);
		TicketBooked.setString(1,ticketBooked.getName());
		TicketBooked.setString(2,ticketBooked.getMail());
		TicketBooked.setString(3,ticketBooked.getPhone_no());
		TicketBooked.setString(4,ticketBooked.getGender());
		TicketBooked.setInt(5,1);
		TicketBooked.setInt(6,Integer.parseInt(passanger_id));  
		TicketBooked.setInt(7,Integer.parseInt(ticketBooked.getTrip_id()));
		TicketBooked.setString(8,ticketBooked.getSeat_id());  
		TicketBooked.executeUpdate();
		

		PreparedStatement InsertIntoBankDetails = conn.prepareStatement(ConstantValues.INSERT_INTO_PASSANGERBANK);
		InsertIntoBankDetails.setString(1,ticketBooked.getCard_number());
		InsertIntoBankDetails.setString(2,ticketBooked.getCard_exp());
		InsertIntoBankDetails.setInt(3,Integer.parseInt(passanger_id));
		InsertIntoBankDetails.executeUpdate();
	    
		json.put(ConstantValues.STATUS, ConstantValues.SUCCESS);
		
		return json;
	}



	public String getTrip_id() {
		return trip_id;
	}


	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}


	public String getCard_number() {
		return card_number;
	}


	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}


	public String getCcv_number() {
		return ccv_number;
	}


	public void setCcv_number(String ccv_number) {
		this.ccv_number = ccv_number;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPhone_no() {
		return phone_no;
	}


	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getSeat_id() {
		return seat_id;
	}


	public void setSeat_id(String seat_id) {
		this.seat_id = seat_id;
	}


	public String getSave_card() {
		return save_card;
	}


	public void setSave_card(String save_card) {
		this.save_card = save_card;
	}


	public String getCard_exp() {
		return card_exp;
	}


	public void setCard_exp(String card_exp) {
		this.card_exp = card_exp;
	}
		
	

	
}
